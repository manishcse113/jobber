const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
//var Serviceman = require('./app/models/JobberSchema.js');
var OtpController = require('./app/controllers/otp.controller.js');

// create express app
const app = express();
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser());

// parse requests of content-type - application/json
app.use(bodyParser.json())

//
var originsWhitelist = [
  'http://localhost:8080',
  'http://127.0.0.1:8080',
  '*',
  'http://52.55.10.96:8080',
  
  //,      //this is my front-end url for development
  // 'http://www.myproductionurl.com'
];
var corsOptions = {credentials: true,
  origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
  },
  credentials:true
}
//here is the magic
app.use(cors(corsOptions));


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});



// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url)
.then(() => {	
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...' + err);
    process.exit();
});

//var db = client.db('jobber');
//exports.db;


// define a simple route
	app.get('/', (req, res) => {
		res.json({"message": "Welcome to Jobber."});
	});
	
require('./app/routes/routes.js')(app);

function intervalFunc() {
  console.log('Regular interval - Cant stop me now!');
  OtpController.clearTimeoutOtp();
}

setInterval(intervalFunc, 60000);

// listen for requests
app.listen(8086, () => {
    console.log("Server is listening on port 8086");
});