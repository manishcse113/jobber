var json2csv = require('json2csv');
 
exports.get = function(req, res) {
 
    var fields = [
		'serviceName',
		'name_first',
		'name_last',
		'location',
		'city',
		'rating',
		'distance',
		'phone',
		'state', 
		'pin', 
		'mail', 
		'like', //0 means no like, 1 means likes
		'response' //
		];
		
	var csv = json2csv({ data: '', fields: fields });
 
    res.set("Content-Disposition", "attachment;filename=ServiceMen.csv");
    res.set("Content-Type", "application/octet-stream");
 
    res.send(csv);
 
};