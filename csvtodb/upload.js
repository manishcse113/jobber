var csv = require('fast-csv');
var mongoose = require('mongoose');
var Serviceman = require('./experts');

exports.post = function (req, res) {
    if (!req.files)
        return res.status(400).send('No files were uploaded.');
     
    var ServicemanFile = req.files.file;
	
	var servicemans = [];
	
	csv
     .fromString(ServicemanFile.data.toString(), {
         headers: true,
         ignoreEmpty: true
     })
	 
	 .on("data", function(data){
         data['_id'] = new mongoose.Types.ObjectId();
          
         servicemans.push(data);
     })
	 
	 .on("end", function(){
         Serviceman.create(servicemans, function(err, documents) {
            if (err) throw err;
         });
		 
	 res.send(servicemans.length + ' Servicemen have been successfully uploaded.');
     });
};