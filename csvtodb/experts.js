var mongoose=require('mongoose');
var Schema = mongoose.Schema;
var ServicemanSchema   = new Schema(
	{
	serviceName: String,
	UserName : String,
	name_first: String,
	name_last: String,
	location: String,
	city:String,
	rating: Number, //how many likes
	phone1: String,
	phone2 : String,
	Address : String,
	state: String, 
	pin: String, 
	mail: String, 
	like: Number, //0 means no like, 1 means likes
    response: Number, //0 mena no response, 1 means response earlier 
	spassword : String
});

var Serviceman=mongoose.model('Serviceman',ServicemanSchema);
module.exports = Serviceman;