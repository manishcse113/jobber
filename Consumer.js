// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080	; // set our port

// DATABASE SETUP
var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
mongoose.connect('mongodb://localhost:27017/Consumers');

// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});

// Consumer Routes
// var Consumer     = require('./app/models/consumer_schema');

var Schema = mongoose.Schema;
var ConsumerSchema   = new Schema({
	Cname_first: String, Cname_last: String, Cadd_1: String, Cadd_2 : String, Cadd_3: String, Ccity: String, Cstate: String, 
	Cpin: String, Cphone: String, Cmail: String, pswd : String
});
var Consumer=mongoose.model('Consumer',ConsumerSchema);
module.exports = Consumer;

mongoose.Promise=require('bluebird');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /bears
// ---------------------------------------------------
router.route('/consumers')

	// create a consumer (accessed at POST http://localhost:8080/consumers)
	.post(function(req, res) {
		
		var consumer = new Consumer();		// create a new instance of the Consumer model
		
		//consumer._id=req.body.phone;
		consumer.Cname_first = req.body.fname;
		consumer.Cname_last = req.body.lname; 
        consumer.Cadd_1 = req.body.add1;
        consumer.Cadd_2 = req.body.add2;
        consumer.Cadd_3 = req.body.add3;
        consumer.Ccity = req.body.city;
        consumer.Cstate = req.body.state;
        consumer.Cpin = req.body.pin;
		consumer.Cphone = req.body.phone;
		consumer.Cmail = req.body.mail;
        consumer.pswd = req.body.pswd ;	
		console.log(req.body.phone);
        
		consumer.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'Consumer created!' });
		});

		
	})

	// get all the consumers (accessed at GET http://localhost:8080/api/consumers)
	.get(function(req, res) {
		Consumer.find(function(err, consumer) {
			if (err)
				res.send(err);
		console.log('Get All');
			res.json(consumer);
		});
	});

		// get all the consumers in a city (accessed at GET http://localhost:8080/api/consumers/city/:city)
	 router.route('/consumers/city/:city')
	.get(function(req, res) {
		console.log(req.params.city);
		Consumer.find({Ccity : req.params.city},function(err, consumer) {
			if (err)
				res.send(err);
			console.log('Getting City..');
			res.json(consumer);
		});
	});
	
	
// on routes that end in /consumers/:consumer_id
// ----------------------------------------------------
//router.route('/consumers/:consumer_id')
//
//	// get the consumer with that id
//	.get(function(req, res) {
//		Consumer.findById(req.params.consumer_id, function(err, consumer) {
//			if (err)
//				res.send(err);
//			res.json(consumer);
//			console.log('Here are you');
//			//console.log(consumer.Cname_first);
//			
//		});
//	})

//	// update the consumer with this id
//	.put(function(req, res) {
//		Consumer.findById(req.params.consumer_id, function(err, consumer) {

//			if (err)
//				res.send(err);

//			consumer.Cname_first = req.body.fname;
//			consumer.Cname_last = req.body.lname;
//			consumer.save(function(err) {
//				if (err)
//					res.send(err);
//
//				res.json({ message: 'Consumer updated!' });
//			});
//
//		});
//	})

	// delete the consumer with this id
//	.delete(function(req, res) {
//		Consumer.remove({
//			_id: req.params.consumer_id
//		}, function(err, consumer) {
//			if (err)
//				res.send(err);

//			res.json({ message: 'Successfully deleted' });
//		});
//	});


// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
