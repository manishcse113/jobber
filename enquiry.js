// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

var cors=require('./cors');
app.use(cors.permission);

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080	; // set our port

// DATABASE SETUP
var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
mongoose.connect('mongodb://localhost:27017/enquiry');


// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});


var Schema = mongoose.Schema;
var EnquirySchema   = new Schema(
	{
	enquiryId: Number,
	enquiry_date : Date,
	status : String,			//A for accepted, P for Pending, C for completed, R for Rejected 
	consumerId : Number,
	jobId : Number,
	servicemanId : Number
});
var Enquiry=mongoose.model('Enquiry',EnquirySchema);
module.exports = Enquiry;

mongoose.Promise=require('bluebird');

		var enquirym = new Enquiry();

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /bears
// ---------------------------------------------------
router.route('/enquiry')

	// create a enquiry (accessed at POST http://localhost:8080/enquiry)
	.post(function(req, res) {
		console.log('Post Enquiry received');
		var enqmax = 0;
		Enquiry.findOne().sort('-enquiryId').exec( function(err, enquirym) {
			console.log('enquiryid=');
			console.log(enquirym.enquiryId);
			enqmax=enquirym.enquiryId + 1;
			console.log('enqmax=');
		console.log(enqmax);
		
		var enquiry=new Enquiry();
		enquiry.enquiryId=enqmax;
		enquiry.enquiry_date = req.body.enquiry_date;
		enquiry.status = req.body.status;
		enquiry.consumerId=req.body.consumerId;
		enquiry.servicemanId=req.body.servicemanId;
		
		console.log(req.body.enquiry_date);

		enquiry.save(function(err) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
				
			res.json({ message: 'Enquiry created!' });

			//res.send(JSON.stringify({a:1}, null, 3));
		});

	 });
	})

	// get all the enquiry (accessed at GET http://localhost:8080/api/enquiry)
	.get(function(req, res) {
		Enquiry.find(function(err, enquiry) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(enquiry);
	
		});
	});


		router.route('/enquiry/consumer/:consumerId')
	.get(function(req, res) {
		console.log(req.params.consumerId);
		Enquiry.find({consumerId : req.params.consumerId},function(err, enquiry) {
			if (err)
				res.send(err);
			console.log('Getting Enquiry for ConnsumerID..');
			res.json(enquiry);
		});
	});
	
		router.route('/enquiry/serviceman/:servicemanId')
	.get(function(req, res) {
		console.log(req.params.servicemanId);
		Enquiry.find({servicemanId : req.params.servicemanId},function(err, enquiry) {
			if (err)
				res.send(err);
			console.log('Getting Enquiry for servicemanID..');
			res.json(enquiry);
		});
	});
	
// update Enquiry Status
	router.route('/enquiry/enquiryId/:status')
	.put(function(req, res) {
		Enquiry.findById(req.params.enquiryId, function(err, enquiry) {

			if (err)
				res.send(err);

			enquiry.status=req.params.status;
			enquiry.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'enquiry Status updated!' });
			});

		});
	})

	// delete the consumer with this id
//	.delete(function(req, res) {
//		Consumer.remove({
//			_id: req.params.consumer_id
//		}, function(err, consumer) {
//			if (err)
//				res.send(err);

//			res.json({ message: 'Successfully deleted' });
//		});
//	});



// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
