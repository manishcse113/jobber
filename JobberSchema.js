// BASE SETUP -Test comment
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080	; // set our port

// DATABASE SETUP
var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
mongoose.connect('mongodb://localhost:27017/Serviceman');

// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});

// Consumer Routes
// var Consumer     = require('./app/models/consumer_schema');

var Schema = mongoose.Schema;
var ServicemanSchema   = new Schema(
	{

	serviceName: String,
	serviceIconPath : String,
	name: String,
	location: String,
	lat: Number,
	lng: Number,
	city:String,
	rating: Number, //how many likes
	distance: Number,
	phone: String,

	name_last: String, 
	state: String, 
	pin: String, 
	mail: String, 
	like: Number, //0 means no like, 1 means likes
    response: Number //0 mena no response, 1 means response earlier 


});
var Serviceman=mongoose.model('Serviceman',ServicemanSchema);
module.exports = Serviceman;

var Favman = require('./app/models/favman');
var Responses = require('./app/models/response');
var Consumer = require('./app/models/consumer');
var Enquiry = require('./app/models/enquiry');

mongoose.Promise=require('bluebird');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// API for Servicemn Begins here 
// ---------------------------------------------------
router.route('/serviceman')

	// create a serviceman (accessed at POST http://localhost:8080/serviceman)
	.post(function(req, res) {
		console.log('Post Serviceman received');
		
		var serviceman = new Serviceman();		// create a new instance of the Consumer model
		
		//consumer._id=req.body.phone;
		serviceman.serviceName = req.body.serviceName;
		serviceman.serviceIconPath  = req.body.serviceIconPath;
		serviceman.name = req.body.name;
		serviceman.location = req.body.location;


		serviceman.lat = req.body.lat;
		serviceman.lng = req.body.lng;


		serviceman.lat = req.body.lat;
		serviceman.lng = req.body.lng;


		serviceman.lat = req.body.lat;
		serviceman.lng = req.body.lng;



		serviceman.city = req.body.city;
		serviceman.rating = req.body.rating; //how many likes
		serviceman.distance = req.body.distance; //shall be calculated
		serviceman.phone = req.body.phone;

		serviceman.name_last = req.body.name_last;
		serviceman.state = req.body.state;
		serviceman.pin = req.body.pin;
		serviceman.mail = req.body.mail;
		serviceman.like = req.body.like ;///: Number, //0 means no like, 1 means likes
		serviceman.response = req.body.response;//: Number //0 mena no response, 1 means response earlier 


		serviceman.save(function(err) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
				
			res.json({ message: 'Serviceman created!' });
			//res.send(JSON.stringify({a:1}, null, 3));
		});

		
	})
	
			// get all the serviceman in a city (accessed at GET http://localhost:8080/api/serviceman/city/:city)
	 router.route('/serviceman/city/:city')
	.get(function(req, res) {
		console.log(req.params.city);
		Serviceman.find({city : req.params.city},function(err, serviceman) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
			console.log('Getting City..');
			
			res.json(serviceman);
			//res.send(JSON.stringify({a:1}, null, 3));
		});
	});
	
    // get all the serviceman in a location
	router.route('/serviceman/location/:location')
	.get(function(req, res) {
		console.log(req.params.location);
		Serviceman.find({locatiion : req.params.location},function(err, serviceman) {
			if (err)
				res.send(err);
			console.log('Getting Location..');
			res.json(serviceman);
		});
	});
	
	// get all the serviceman in a pin
		router.route('/serviceman/pin/:pin')
	.get(function(req, res) {
		console.log(req.params.pin);
		Serviceman.find({pin : req.params.pin},function(err, serviceman) {
			if (err)
				res.send(err);
			console.log('Getting Pin..');
			res.json(serviceman);
		});
	});
	

	router.route('/consumer')
	// create a consumer (accessed at POST http://localhost:8080/consumers)
	.post(function(req, res) {
		
		Consumer.findOne({consumerId:req.body.consumerId}, function(err,consumer1) {
		if(err)
		{
		return err;
		}
		if(!consumer1)
		{
		var consumer = new Consumer();		// create a new instance of the Consumer model
		consumer.consumerId=req.body.consumerId;
		//consumer._id=req.body.phone;
		consumer.Cname_first = req.body.Cname_first;
		consumer.Cname_last = req.body.Cname_last; 
        consumer.Cadd_1 = req.body.Cadd_1;
        consumer.Cadd_2 = req.body.Cadd_2;
        consumer.Cadd_3 = req.body.Cadd_3;
        consumer.Ccity = req.body.Ccity;
        consumer.Cstate = req.body.Cstate;
        consumer.Cpin = req.body.Cpin;
		consumer.Cphone = req.body.Cphone;
		consumer.Cmail = req.body.Cmail;
        consumer.Cpswd = req.body.Cpswd ;	
		console.log(req.body.Cphone);
   9     
		consumer.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'Consumer created!' });
		});

		}
		else
			res.json({ message: 'ConsumerId already exists!' });
	});
	})
	
		//get all Consumers
		router.route('/consumer')
	.get(function(req, res) {
		console.log(req.params.consumerId);
		Consumer.find(function(err, consumer) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
			console.log('Getting User..');
			res.json(consumer);
		});
	});
	
		//To check if a ConsumerID exits and return the record
	router.route('/consumer/:consumerId')
	.get(function(req, res) {
		console.log(req.params.consumerId);
		Consumer.find({consumerId:req.params.consumerId},function(err, consumer) {
			if (err)
				res.send(err);
			console.log('Getting Consumer..');
			res.json(consumer);
		});
	});
	
	
	//get User from the table if password matches
		router.route('/consumer/:consumerId/:password')
	.get(function(req, res) {
		console.log(req.params.consumerId);
		Consumer.find({consumerId:req.params.consumerId, Cpswd:req.params.pswd},function(err, consumer) {
			if (err)
				res.send(err);
			console.log('Getting User..');
			res.json(consumer);
		});
	});
	
		
	//update favourite serviceman by consumer for a service
	router.route('/favman')
	.post(function(req,res) {
		var favman = new Favman();
		console.log(req.body.consumerId);
		var mfind=1;
		
			Favman.findOne({consumerId:req.body.consumerId, servicemanId : req.body.servicemanId, serviceName : req.body.serviceName}, function(err,favman1) {
			if(err)
			{
			return err;
			}
			if(!favman1)
			{
			var favman = new Favman();
			favman.consumerId=req.body.consumerId;
			favman.servicemanId=req.body.servicemanId;
			favman.city=req.body.city;
			favman.location=req.body.location;
			favman.serviceName=req.body.serviceName;
			favman.like=1;
			
	
			favman.save(function(err) {
				res.setHeader('Content-Type', "application/json")
				if (err)
				res.send(err);
				
				res.json({ message: 'Favourite Serviceman created!' });
;
			//res.send(JSON.stringify({a:1}, null, 3));
			});
			}
			else
			res.json({ message: 'Favourite Serviceman already exists!' });
			});
	});
	
		// delete the Favourite serviceman for a consumer
		router.route('/favman/:consumerId/:servicemanId/:serviceName')
		.delete(function(req, res) {
			Favman.findOneAndRemove({consumerId: req.params.consumerId, servicemanId : req.params.servicemanId, serviceName :req.params.serviceName}, function(err, favman) {
				if (err)
					res.send(err);

				res.json({ message: 'Successfully deleted' });
			});
		});

	//get the Favourite servicemanId for consumer-city-location-service 
	router.route('/favman/:consumerId/:city/:location/:serviceName')
	.get(function(req, res) {
		Favman.find({consumerId : req.params.consumerId, city : req.params.city, location : req.params.location, serviceName: req.params.serviceName},function(err, favman) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(favman);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});
	
	//get the Favourite servicemanId for consumer
	router.route('/favman/:consumerId')
	.get(function(req, res) {
		Favman.findOne({consumerId : req.params.consumerId},function(err, favman) {
			//res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log(favman.like);
		//console.log(req);
			res.json(favman);
			console.log(favman.like);
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});

	router.route('/favman')
	.get(function(req, res) {
		Favman.find(function(err, favman) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(favman);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});
	
	
	//update response by serviceman for consumer for a job
	router.route('/response')
	.post(function(req,res) {
		console.log(req.body.consumerId);
				
			Responses.findOne({consumerId:req.body.consumerId, servicemanId : req.body.servicemanId, jobId : req.body.jobId}, function(err,response1) {
			if(err)
			{
			return err;
			}
			if(!response1)
			{
			var responses = new Responses();
			responses.consumerId=req.body.consumerId;
			responses.servicemanId=req.body.servicemanId;
			responses.city=req.body.city;
			responses.location=req.body.location;
			responses.serviceName=req.body.serviceName;
			responses.response=1;
			responses.jobId=req.body.jobId;
	
			responses.save(function(err) {
				res.setHeader('Content-Type', "application/json")
				if (err)
				res.send(err);
				
				res.json({ message: 'Serviceman Response created!' });
;
			//res.send(JSON.stringify({a:1}, null, 3));
			});
			}
			else
			res.json({ message: ' Serviceman Response already exists!' });
			});
	});
	
		//get the Serviceman who responded for consumer-city-service 
	router.route('/response/:consumerId/:city/:serviceName')
	.get(function(req, res) {
		Responses.find({consumerId : req.params.consumerId, city : req.params.city, serviceName: req.params.serviceName},function(err, responses) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(responses);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});
	
		router.route('/response/:consumerId/')
	.get(function(req, res) {
		Responses.find({consumerId : req.params.consumerId},function(err, responses) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req)
			res.json(responses);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});
	
			router.route('/response/:consumerId/:jobId')
	.get(function(req, res) {
		Responses.find({consumerId : req.params.consumerId, jobId:req.params.jobId},function(err, responses) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(responses);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});
	
	//get all list from responses collection
	router.route('/response')
	.get(function(req, res) {
		Responses.find(function(err, responses) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(responses);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});
	
	
	// get all the serviceman (accessed at GET http://localhost:8080/api/serviceman)
	router.route('/serviceman')
	.get(function(req, res) {
		Serviceman.find(function(err, serviceman) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(serviceman);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});


	
	// get all the serviceman for response
	
				router.route('/serviceman/response/:response')
	.get(function(req, res) {
		console.log(req.params.response);
		Serviceman.find({response : req.params.response},function(err, serviceman) {
			if (err)
				res.send(err);
			console.log('Getting Response..');
			res.json(serviceman);
		});
	});
	
	// get all the serviceman in a category, city and location
	 router.route('/serviceman/:serviceName/:city/:location')

	.get(function(req, res) {
		console.log(req.params.city);
		Serviceman.find({city : req.params.city, serviceName : req.params.serviceName, location : req.params.location},function(err, serviceman) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
			console.log('Getting City and servicename..');
			res.json(serviceman);
			
			//res.send(JSON.stringify({a:1}, null, 3));
		});
	});
	
			// get all the serviceman in a category (accessed at GET http://localhost:8080/api/serviceman/serviceName/:serviceName)
	 router.route('/serviceman/serviceName/:serviceName')
	.get(function(req, res) {
		console.log(req.params.city);
		Serviceman.find({serviceName : req.params.serviceName},function(err, serviceman) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
			console.log('Getting serviceName..');
			res.json(serviceman);
			
			//res.send(JSON.stringify({a:1}, null, 3));
		});
	});
	
	
// on routes that end in /consumers/:consumer_id
// ----------------------------------------------------
router.route('/serviceman/:serviceman_id')
//
//	// get the consumer with that id
	.get(function(req, res) {
		Serviceman.findById(req.params.serviceman_id, function(err, serviceman) {
			if (err)
				res.send(err);
			res.json(serviceman);
			console.log('Here are you');
//			//console.log(consumer.Cname_first);
			
		});
	})


// ----- API for Enquiry Begins Here -------------

router.route('/enquiry')

	// create a enquiry (accessed at POST http://localhost:8080/enquiry)
	.post(function(req, res) {
		console.log('Post Enquiry received');
		var enqmax = 0;
		Enquiry.findOne(function(err,enquiry1) {
		if(err)
		{
		return err;
		}
		if(enquiry1)
		{		
		Enquiry.findOne().sort('-enquiryId').exec( function(err, enquirym) {
		enqmax=enquirym.enquiryId + 1;
		console.log('enqmax=');
		console.log(enqmax);
		var enquiry=new Enquiry();
		enquiry.enquiryId=enqmax;
		enquiry.enquiry_date = req.body.enquiry_date;
		enquiry.status = req.body.status;
		enquiry.consumerId=req.body.consumerId;
		enquiry.servicemanId=req.body.servicemanId;
		enquiry.jobId=req.body.jobId;
		
		console.log(req.body.enquiry_date);

		enquiry.save(function(err) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
				
			res.json({ message: 'Enquiry created!' });

			//res.send(JSON.stringify({a:1}, null, 3));
		});

		});
		}
		else
		{
		var enquiry=new Enquiry();
		enquiry.enquiryId=1;
		enquiry.enquiry_date = req.body.enquiry_date;
		enquiry.status = req.body.status;
		enquiry.consumerId=req.body.consumerId;
		enquiry.servicemanId=req.body.servicemanId;
		enquiry.jobId=req.body.jobId;
		
		console.log(req.body.enquiry_date);

		enquiry.save(function(err) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
				
			res.json({ message: 'Enquiry created!' });

			//res.send(JSON.stringify({a:1}, null, 3));
		});

		}
		
	});
	})
	// get all the enquiry (accessed at GET http://localhost:8080/api/enquiry)
	.get(function(req, res) {
		Enquiry.find(function(err, enquiry) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(enquiry);
	
		});
	});


		router.route('/enquiry/consumer/:consumerId')
	.get(function(req, res) {
		console.log(req.params.consumerId);
		Enquiry.find({consumerId : req.params.consumerId},function(err, enquiry) {
			if (err)
				res.send(err);
			console.log('Getting Enquiry for ConnsumerID..');
			res.json(enquiry);
		});
	});
	
		router.route('/enquiry/serviceman/:servicemanId')
	.get(function(req, res) {
		console.log(req.params.servicemanId);
		Enquiry.find({servicemanId : req.params.servicemanId},function(err, enquiry) {
			if (err)
				res.send(err);
			console.log('Getting Enquiry for servicemanID..');
			res.json(enquiry);
		});
	});
	
// update Enquiry Status
	router.route('/enquiry/enquiryId/:status')
	.put(function(req, res) {
		Enquiry.findById(req.params.enquiryId, function(err, enquiry) {

			if (err)
				res.send(err);

			enquiry.status=req.params.status;
			enquiry.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'enquiry Status updated!' });
			});

		});
	})
	
	
	
	
	// update the like for serviceman ID
//	router.route('/serviceman/:serviceman_id')
	.put(function(req, res) {
		Serviceman.findById(req.params.serviceman_id, function(err, serviceman) {

			if (err)
				res.send(err);

			serviceman.like=1;
			serviceman.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Consumer updated!' });
			});

		});
	})

	// delete the consumer with this id
//	.delete(function(req, res) {
//		Consumer.remove({
//			_id: req.params.consumer_id
//		}, function(err, consumer) {
//			if (err)
//				res.send(err);

//			res.json({ message: 'Successfully deleted' });
//		});
//	});



// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
