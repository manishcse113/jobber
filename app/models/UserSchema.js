var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ConsumerSchema = new Schema({
  userName: String,
  password: String,
  phone: String,
  email: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  firstName: String,
  lastName: String,
  isVerified: Boolean,
  photoUrl: String
}, {   timestamps: true}
);
var Consumer = mongoose.model('Consumers', ConsumerSchema, );
module.exports = Consumer;