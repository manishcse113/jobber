var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DeviceLastEventSchema = new Schema({

  macId: String,
  date: String,
  month: String,
  year: String,
  hour: String,
  minute: String,
  second: String,
  eventId: String,

  
}, {   timestamps: true}
);
var DeviceLastEvent = mongoose.model('DeviceLastEvents', DeviceLastEventSchema, );
module.exports = DeviceLastEvent;