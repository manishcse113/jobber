var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DeviceProfileSchema = new Schema({
  macId: String,
  morStartHr: String,
  morStartMin: String,
  
  eveStartHr: String,
  eveStartMin: String,
  retries:String,
  retryInterval: String,
  noWaterStopTime:String


}, {   timestamps: true}
);
var DeviceProfile = mongoose.model('DeviceProfiles', DeviceProfileSchema, );
module.exports = DeviceProfile;