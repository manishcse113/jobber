var mongoose     = require('mongoose');
//mongoose.connect('mongodb://localhost:27017/favman');
//module.exports=exports=mongoose;

var Schema       = mongoose.Schema;

var favmanSchema   = new Schema({
	consumerId : String, servicemanId : String, like : Number, city : String, Location : String, serviceName : String
});

module.exports = mongoose.model('favman', favmanSchema);