var mongoose     = require('mongoose');
//mongoose.connect('mongodb://localhost:27017/favman');
//module.exports=exports=mongoose;

var Schema       = mongoose.Schema;

var responseSchema   = new Schema({
	consumerId : String, servicemanId : String, response : Number, city : String, Location : String, serviceName : String, jobId : Number
});

module.exports = mongoose.model('response', responseSchema);