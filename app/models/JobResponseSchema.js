var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var JobResponseSchema = new Schema({
  jobId:String,
  senderId: String,
  senderPhone: String,
  senderUserName: String,
  senderFirstName: String,
  isConsumer: String,
  msg: String,
  parentResponseId: String,

 }, 
 {   timestamps: true}
);
var Response = mongoose.model('Response', JobResponseSchema );
module.exports = Response;