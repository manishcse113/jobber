var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var JobSchema = new Schema({
  serviceName: String,
  title: String,
  description: String,
  userId: String,
  userName: String,
  userFirstName: String,
  userLastName: String,
  userCity: String,
  userLocation: String,
  userPhone: String,
  serviceDate: String,
  serviceTime: String,
  status: String
 }, 
 {   timestamps: true}
);
JobSchema.index(
	   { "title": "text", "description": "text" },
	   { "weights": {  "title": 10 } }
	);
var Job = mongoose.model('Job', JobSchema, );
module.exports = Job;