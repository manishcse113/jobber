var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var QuestionResultSchema = new Schema({
  userId: String,
  testId: String,
  qId: String,
  testLanguage: String,
  question: String,
  gt: String,
  ut: String,
  marks: String,
  remarks: String,
  userTestNumber: String
}, {   timestamps: true}
);
var QuestionResult = mongoose.model('QuestionResult', QuestionResultSchema, );
module.exports = QuestionResult;