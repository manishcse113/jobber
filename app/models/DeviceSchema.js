var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DeviceSchema = new Schema({
  phone: String,
  macId: String,
  deviceName: String,
  osVersion: String,
  appSwVersion: String,
  hwVersion: String
  
}, {   timestamps: true}
);
var Device = mongoose.model('Devices', DeviceSchema, );
module.exports = Device;