var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SManFavouriteSchema = new Schema({
  userName: String,
  serviceName: String,
  password: String,
  phone: String,
  email: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  firstName: String,
  lastName: String,
  isVerified: Boolean,
  likeCount: Number,
  photoUrl: String
}, {   timestamps: true}
);
var SmanFav = mongoose.model('ServiceManFacourite', SManFavouriteSchema, );
module.exports = SmanFav;