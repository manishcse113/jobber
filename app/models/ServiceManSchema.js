var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SManSchema = new Schema({
  userName: String,
  serviceName: String,
  password: String,
  phone: String,
  email: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  firstName: String,
  lastName: String,
  isVerified: Boolean,
  likeCount: Number,
  photoUrl: String,
  likedby: [{userName: String, _id: String}]
}, {   timestamps: true}
);
var Sman = mongoose.model('ServiceMan', SManSchema, );
module.exports = Sman;