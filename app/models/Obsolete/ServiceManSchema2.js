var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Schema = mongoose.Schema;
var ServicemanSchema   = new Schema(
	{

	serviceName: String,
	serviceIconPath : String,
	name: String,
	location: String,
	lat: Number,
	lng: Number,
	city:String,
	rating: Number, //how many likes
	distance: Number,
	phone: String,

	name_last: String, 
	state: String, 
	pin: String, 
	mail: String, 
	like: Number, //0 means no like, 1 means likes
    response: Number //0 mena no response, 1 means response earlier  
});
var Serviceman=mongoose.model('Serviceman',ServicemanSchema2);
module.exports = Serviceman;