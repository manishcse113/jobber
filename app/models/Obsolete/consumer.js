var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ConsumerSchema   = new Schema({
	consumerId : String, Cname_first: String, Cname_last: String, Cadd_1: String, Cadd_2 : String, Cadd_3: String, Ccity: String, Cstate: String, 
	Cpin: String, Cphone: String, Cmail: String, Cpswd : String
});

module.exports = mongoose.model('Consumer', ConsumerSchema);