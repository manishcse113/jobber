
// Configuring the database
//const dbConfig = require('./config/database.config.js');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SManSchema = new Schema({
  userName: String,
  serviceName: String,
  password: String,
  phone: String,
  email: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  firstName: String,
  lastName: String,
  likeCount: Number,
  photoUrl: String
}, {   timestamps: true}
);

var Schema = mongoose.Schema;
var ServicemanSchema   = new Schema(
	{

	serviceName: String,
	serviceIconPath : String,
	name: String,
	location: String,
	lat: Number,
	lng: Number,
	city:String,
	rating: Number, //how many likes
	distance: Number,
	phone: String,

	name_last: String, 
	state: String, 
	pin: String, 
	mail: String, 
	like: Number, //0 means no like, 1 means likes
    response: Number //0 mena no response, 1 means response earlier  
});
var Serviceman=mongoose.model('Serviceman',ServicemanSchema);
module.exports = Serviceman;


// compile schema to model

var ConsumerSchema = new Schema({
  userName: String,
  phone: String,
  password: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  email: String,			  
  firstName: String,
  lastName: String,
  photoUrl: String			  
}, {   timestamps: true}
);
// compile schema to model

var OtpSchema = new Schema({
  otpStr: String,
  phone: String,
  userName: String		  
}, {   timestamps: true}
);
// compile schema to model

var FavouriteSchema = new Schema({
  uId: String,
  sManId: String
}, {   timestamps: true}
);
// compile schema to model

var EnquirySchema = new Schema({
  
  userId: String,
  userLocation: String,
  userPhone: String,
  userCity: String,
  sManId: String,
  enquiryMsg: String
}, {   timestamps: true}
);
// compile schema to model


var JobSchema = new Schema({
  title: String,
  description: String,
  uId: String,
  state: String,
  city: String,
  location: String,
  phone: String
}, {   timestamps: true}
);
// compile schema to model

var JobResponseSchema = new Schema({
  jobId: String,
  uId: String,
  response: String,
  parentResponseId: String 
}, {   timestamps: true}
);
// compile schema to model


var Sman = mongoose.model('Sman', SManSchema, );
module.exports = Sman;

var Consumer = mongoose.model('Consumer', ConsumerSchema, );
module.exports = Consumer;

var Otp= mongoose.model('Otp', OtpSchema, );
module.exports = Otp;

var Favourite= mongoose.model('Favourite', FavouriteSchema, );
module.exports = Favourite;

var Enquiry = mongoose.model('Enquiry', EnquirySchema, );
module.exports=Enquiry;

var Job = mongoose.model('Job', JobSchema, );
module.exports=Job;

var JobResponse = mongoose.model('JobResponse', JobResponseSchema, );			
module.exports = JobResponse;

