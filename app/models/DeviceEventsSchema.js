var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DeviceEventSchema = new Schema({

  macId: String,
  date: String,
  month: String,
  year: String,
  hour: String,
  minute: String,
  second: String,
  eventId: String,

  
}, {   timestamps: true}
);
var DeviceEvent = mongoose.model('DeviceEvents', DeviceEventSchema, );
module.exports = DeviceEvent;