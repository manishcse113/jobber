var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FavouriteSchema = new Schema({
  userId: String,
  smanId: String
 }, 
 {   timestamps: true}
);
var Favourite = mongoose.model('Favourite', FavouriteSchema, );
module.exports = Favourite;