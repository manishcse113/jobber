var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OtpSchema = new Schema({
  phone: String,
  otp: String
 }, 
 {   timestamps: true}
);
var Otp = mongoose.model('Otp', OtpSchema, );
module.exports = Otp;