var mongoose     = require('mongoose');
var Schema = mongoose.Schema;
var Sman = require('./ServiceManSchema.js');

var EnquirySchema   = new Schema(
	{
	enquiryId: Number,
	enquiry_date : Date,
	status : String,			//A for accepted, P for Pending, C for completed, R for Rejected 
	consumerId : String,
	jobId : Number,
	servicemanId : String,
	//sman: { type: Schema.Types.ObjectId, ref: 'Sman' },
});

module.exports = mongoose.model('enquiry', EnquirySchema);