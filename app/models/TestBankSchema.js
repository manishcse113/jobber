var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var TestBankSchema = new Schema({
  testName: String,
  q1: String,
  q2: String,
  q3: String,
  q4: String,
  q5: String,
  q6: String,
  q7: String,
  q8: String,
  q9: String,
  q10: String,
  t1: String,
  t2: String,
  t3: String,
  t4: String,
  t5: String,
  t6: String,
  t7: String,
  t8: String,
  t9: String,
  t10: String
}, {   timestamps: true}
);
var TestBank = mongoose.model('TestBank', TestBankSchema, );
module.exports = TestBank;