var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Sman = require('./ServiceManSchema.js');
var EnquirySchema = new Schema({
  userId: String,
  userLocation: String,
  userPhone: String,
  userCity: String,
  serviceManId: mongoose.Schema.Types.ObjectId,
  msg: String,
  sman: { type: Schema.Types.ObjectId, ref: 'ServiceMan' }
 }, 
 {   timestamps: true}
);
var Enquiry = mongoose.model('Enquiry', EnquirySchema, );
module.exports = Enquiry;