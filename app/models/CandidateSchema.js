var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CandidateSchema = new Schema({
  title: String,
  description: String,
  skills:String,
  firstName: String,
  lastName: String,
  
  phone: String,
  email: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  
  photoUrl: String,
  profileUrl: String

 }, {   timestamps: true}
);
var Candidate = mongoose.model('Candidate', CandidateSchema, );
module.exports = Candidate;