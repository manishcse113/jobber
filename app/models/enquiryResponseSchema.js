var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var EnquiryResponseSchema = new Schema({
  enquiryId:String,
  senderId: String,
  senderPhone: String,
  senderUserName: String,
  senderFirstName: String,
  isConsumer: String,
  msg: String,
  parentResponseId: String,

 }, 
 {   timestamps: true}
);
var Response = mongoose.model('EnquiryResponse', EnquiryResponseSchema );
module.exports = Response;