// call the packages we need
var express    = require('express');
var path = require('path');     //used for file path
var fs =require('fs-extra');    //File System-needed for renaming file etc
var multer  = require('multer');
var upload6 = multer({ dest: 'upload/'});
var fs = require('fs');
var mime=require('mime-types');


// ROUTES FOR OUR API
// =============================================================================

module.exports = (app) => {
	var sman = require('../controllers/sman.controller.js');
	//ServiceMan Related
	app.post('/ServiceMan/', sman.create); //Tested //Body includes:serviceName/:userName/:password/:phone
	app.get('/ServiceMan/:phone/:password', sman.login);//tested ok:: Params includes:phone/:password/
    app.get('/ForgetByServiceManName/:userName', sman.forgetByName); //tested OK : http://localhost:3000/ForgetBySManName/ram22/
	app.get('/ForgetByServiceManPhone/:phone', sman.forgetByPhone); //tested OK : http://localhost:3000/ForgetBySManPhone/1234567890/
	app.post('/ServiceManReset/:phone',sman.resetPassword);// //body: oldPassword, newPassword
	app.get('/SearchServiceMan/:city', sman.findByCity);//tested Ok: http://localhost:3000/SearchSMan/Chandigarh
	app.get('/SearchServiceManProf/:city/:serviceName', sman.findByCityProf);//tested ok http://localhost:3000/SearchSManProf/Chandigarh/Plumber
	app.get('/SearchServiceManLocation/:city/:serviceName/:location', sman.findByCityProfLoc); //tested ok: http://localhost:3000/SearchSManLocation/Chandigarh/Plumber/Sector46
	app.get('/SearchAllbyLocation/:city/:location', sman.findAllByLocation); // tested ok: http://localhost:3000/SearchAllbyLocation/Chandigarh/Sector46
	app.get('/SearchAll/', sman.findAll); // tested ok: Works Fine
	app.post('/updateServiceManProfile/:sManId/', sman.updateProfile); //tested ok: http://localhost:3000/updateSManProfile/5ad6c01a2bfd552b4c3a0bc1 //body includes :state/:city:/location:/:pin/:email/:firstName:/lastName
	app.post('/updateLike/:sManId/', sman.updateLike);//tested ok: body like:true/false

	//Not needed
	//app.post('/Service/', sman.createService);
	//app.get('/Serviceman/:phone', sman.login2);//tested ok:: Params includes:phone/	
	//app.get('/SearchAllbyCity/:city', sman.findAllByLocation);
	
	//ConsumerRelated::
	var consumer = require('../controllers/consumer.controller.js');
	app.post('/Consumer/', consumer.create);
	app.get('/Consumer/:phone/:password', consumer.login);
	app.get('/Consumer/', consumer.findAll);
	app.delete('/Consumer/:phone', consumer.deleteByPhone);
	
    app.get('/ForgetByConsumerName/:userName', consumer.forgetByName);
	app.get('/ForgetByConsumerPhone/:phone', consumer.forgetByPhone);
	app.post('/ConsumerReset/:phone',consumer.resetPassword);// //body: oldPassword, newPassword
	app.post('/updateConsumerProfile/:consumerId/', consumer.updateProfile); 
			//tested ok: http://localhost:3000/updateUserProfile/5ad6c01a2bfd552b4c3a0bc1 
			//body includes :state/:city:/location:/:pin/:email/:firstName:/lastName
			
	//Candidate Profile Related:
	var candidate = require('../controllers/candidate.controller.js');
	app.post('/Candidate/', candidate.create);
	app.get('/Candidate/', candidate.findAll);
	app.post('/updateCandidateProfile/:phone/', candidate.updateProfile);
	
	
	var Otp = require('../controllers/otp.controller.js');
	//Otp Related
    app.post('/Validate/', Otp.create);//body contains phone, userRole==Consumer or Serviceman
    app.get('/Validate/:phone/:userRole/:otp', Otp.validate);
	//TBD: auto Clear of OTP after 3 minutes is pending::
	
	var Favourite = require('../controllers/favourite.controller.js');
	//Favourite Related
    app.post('/SmanUserMap/', Favourite.addFavourite); //body : :userId/:sManId
	app.get('/SmanUserMap/:userId', Favourite.getFavourite);
	app.delete('/SmanUserMap/:userId/:smanId', Favourite.deleteFavourite);
	
	var Enquiry = require('../controllers/enquiry.controller.js');	
	//Enquiry Related
    app.post('/Enquiry/', Enquiry.addEnquiry);//body :userId smanId, msg
	app.get('/EnquiryByUser/:userId', Enquiry.getEnquiryByMe);
	app.get('/EnquiryForSMan/:sManId', Enquiry.getEnquiryForSMan);
	
	var Job = require('../controllers/job.controller.js');
	//Job Related
    app.post('/Job/', Job.addJob);//body contains: userId, city, location, phone, serviceName, title, description, date, time,
    app.post('/JobUpdate/:jobId', Job.updateJob);//body contains: userId, city, location, phone, serviceName, title, description, date, time, status

	app.get('/JobsByUser/:userId', Job.getJobsByMe);
	app.get('/JobsByUser/:userId/:status', Job.getJobsByMeStatus);
	
	app.get('/Job/:jobId', Job.getJobsByJobId);
	app.get('/JobByKey/:keys', Job.getJobsByKeyWord);
	
	app.get('/JobsByCity/:city', Job.getJobsByCity);
	app.get('/JobsByCity/:city/:status', Job.getJobsByCityStatus);
	
	app.get('/JobsByCityLocation/:city/:location', Job.getJobsByCityLocation);
	app.get('/JobsByCityLocation/:city/:location/:status', Job.getJobsByCityLocationStatus);
	
	app.get('/JobsByCityService/:city/:serviceName', Job.getJobsByCityService);
	app.get('/JobsByCityService/:city/:serviceName/:status', Job.getJobsByCityServiceStatus);
	
	app.get('/JobsByCityLocationService/:city/:location/:serviceName', Job.getJobsByCityLocationService);
	app.get('/JobsByCityLocationService/:city/:location/:serviceName/:status', Job.getJobsByCityLocationServiceStatus);
	
	
	//Job Response Related
    var response = require('../controllers/jobResponse.controller.js');
	app.post('/Response/', response.addResponse);//body contains: jobId, senderId, senderUserName, senderFirstName, senderPhone, isConsumer, msg, parentResponseId
	app.get('/ResponsesByMe/:senderId', response.getResponseByMe);
	app.get('/ResponsesByJob/:jobId', response.getResponseByJob);
	app.get('/ChildResponses/:responseId', response.getChildResponses);
	
	//Enquriry Response Related
	var enquiryResponse = require('../controllers/enquiryResponse.controller.js');
    app.post('/EnquiryResponse/', enquiryResponse.addResponse);//body contains: enquiryIdId, senderId, senderUserName, senderFirstName, senderPhone, isConsumer, msg, parentResponseId
	app.get('/EnquiryResponsesByMe/:senderId', enquiryResponse.getResponseByMe);
	app.get('/EnquiryResponses/:enquiryId', enquiryResponse.getResponseByEnquiry);
	app.get('/EnquiryChildResponses/:responseId', enquiryResponse.getChildResponses);
	

	//var file = require('../controllers/file.controller.js');
	//app.get("/Download", file.download );
	//app.post("/Upload", type, file.upload) ;	
	
	//testBank Related::
	var testBank = require('../controllers/testbank.controller.js');
	app.post('/TestBank/', testBank.create); //Tested: working fine
	app.get('/TestBank/', testBank.getRandom); //Testing failed:
	app.get('/TestBank/:testName/:language', testBank.getByName); //Tested: working fine: http:\\localhost:8080/TestBank/Housing2
	app.get('/TestBankAllNames/', testBank.getAllNames); //Tested: working fine: http:\\localhost:8080/TestBank/Housing2
    app.get('/TestBankAll/', testBank.getAll); //Tested: working fine
	
	//QuestionResult Related::
	var questionResult = require('../controllers/questionResult.controller.js');
	app.post('/QuestionResult/', questionResult.create); ////tested: success
	app.post('/QuestionResultUpdate/', questionResult.updateQResult); //
	app.post('/QuestionResultUpdateGt/', questionResult.updateQResultGt); //
	app.post('/QuestionResultUpdateMarks/', questionResult.updateQResultMarks); //
		
	app.get('/questionResult/:userId', questionResult.getByUserId); //tested: success
	app.get('/questionResult/:userId/:testId', questionResult.getByUserTestId); //tested: success
	app.get('/questionResult/:userId/:testId/:userTestNumber', questionResult.getByUserTestNumber); //tested: success
    app.get('/questionResult/', questionResult.getAll); ////tested: success
	
	
	//Device Related::
	var device = require('../controllers/device.controller.js');
	app.post('/device/', device.addDevice); ////tested: success
	app.get('/device/', device.getAllDevices); //tested: success
	app.get('/device/:macId', device.getDevicebyMacId); //tested: success
	app.get('/devicebyPhone/:phone', device.getDevicebyPhone); //tested: success

	//DeviceEvent Related::
	var deviceEvent = require('../controllers/deviceevent.controller.js');
	app.post('/deviceEvent/', deviceEvent.addDeviceEvent); ////tested: success
	app.get('/deviceEvent/', deviceEvent.getAllEvents); //tested: success
	app.get('/deviceEvent/:macId', deviceEvent.getDeviceEventsbyMacId); //tested: success
	
	//DeviceLastEvent Related::
	var deviceLastEvent = require('../controllers/devicelastevent.controller.js');
	app.post('/deviceLastEvent/', deviceLastEvent.addDeviceEvent); ////tested: success
	app.get('/deviceLastEvent/', deviceLastEvent.getAllEvents); //tested: success
	app.get('/deviceLastEvent/:macId', deviceLastEvent.getDeviceEventsbyMacId); //tested: success

	//DeviceProfile Related::
	var deviceProfile = require('../controllers/deviceprofile.controller.js');
	app.post('/deviceProfile/', deviceProfile.addDeviceProfile); ////tested: success
	app.get('/deviceProfile/', deviceProfile.getAllDeviceProfiles); //tested: success
	app.get('/deviceProfile/:macId', deviceProfile.getDeviceProfilebyMacId); //tested: success
	
	typeConsumer : String;
	typeServiceman: String;
	subPathProfile: String;
	
	typeConsumer = "Consumer";
	typeServiceman = "ServiceMan"
	typeCandidate = "Candidate"
	subPathProfile ="profile"
	

	
/** Permissible loading a single file, 
	the value of the attribute "key" in the form of "image". **/
	var type = upload6.single('image');
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'ejs');

	app.post('/file/:senderId/:subPath/:contextId', type, function (req,res) 
	//exports.upload = (req, res) =>
	{
		console.log('Upload');
		/** When using the "single"
		  data come in "req.file" regardless of the attribute "name". **/
		var tmp_path = req.file.path;
		console.log('path' + req.file.path)

		//sent by - in the context of userId, serviceManId..
		if(!req.params.senderId) {
			return res.status(400).send({
				message: "upload create -senderId can not be empty"
			});
		}
		
		//subPath can be profile, chat, job, response, enquiry etc.
		if(!req.params.subPath) {
			return res.status(400).send({
				message: "upload create -subPath can not be empty"
			});
		}
		
			//sent in the sub-context of userId, serviceManId, jobId, responseId, chatId, enquiryId etc.
		if(!req.params.contextId) {
			return res.status(400).send({
				message: "upload create -contextId can not be empty"
			});
		}
	
		//Save metadata information about this fie in sub-context::
		
		//Create folder - if doesn't exists:
		if (!fs.existsSync('upload/' + req.params.senderId)){
			fs.mkdirSync('upload/' + req.params.senderId);
		}
		
		if (!fs.existsSync('upload/' + req.params.senderId + '/' + req.params.subPath)){
			fs.mkdirSync('upload/' + req.params.senderId + '/' + req.params.subPath);
		}
		
		
		/** The original name of the uploaded file
		  stored in the variable "originalname". **/
		var target_path = 'upload/' + req.params.senderId + '/' + req.params.subPath + '/' + req.file.originalname;
		/** A better way to copy the uploaded file. **/
		console.log('Origfilename## ' + req.file.originalname + 'TargetPath## ' + target_path)

		var src = fs.createReadStream(tmp_path);
		var dest = fs.createWriteStream(target_path);
		src.pipe(dest);
		src.on('end', function() { 
			//res.render('complete'); 
			console.log("End");
			
			if ((req.params.senderId===typeConsumer) && (req.params.subPath===subPathProfile))
			{
				consumer.updateProfilePic(req, res);
			}
			else if ((req.params.senderId===typeServiceman) && (req.params.subPath===subPathProfile))
			{
				sman.updateProfilePic(req, res);
			}
			else if ((req.params.senderId===typeCandidate) && (req.params.subPath===subPathProfile))
			{
				candidate.updateProfilePic(req, res);
			}
			
			else
			{
				res.send();
			}
		});
		src.on('error', function(err) { 
			console.log("error");
			res.send();
			//res.render('error'); 
		});
	})
	;
	
	//var path = require('path');
	//var mime = require('mime');
	//var fs = require('fs');
	app.get('/file/:fileName/:senderId/:subPath/:contextId', function(req, res){
		console.log('download request');  
		//sent by - in the context of userId, serviceManId..
		if(!req.params.senderId) {
			return res.status(400).send({
				message: "upload create -senderId can not be empty"
			});
		}		
		//subPath can be profile, chat, job, response, enquiry etc.
		if(!req.params.subPath) {
			return res.status(400).send({
				message: "upload create -subPath can not be empty"
			});
		}		
		//sent in the sub-context of userId, serviceManId, jobId, responseId, chatId, enquiryId etc.
		if(!req.params.contextId) {
			return res.status(400).send({
				message: "upload create -contextId can not be empty"
			});
		}
		var file = 'upload/' + req.params.senderId + '/' + req.params.subPath + '/' + req.params.fileName;
		console.log('filepath ## ' + file);
		var filename = path.basename(file);
		console.log('##fileName## ' + filename);
		
		if (!fs.existsSync(file)){
			return res.status(400).send({
				message: "File doesn't exists" + filename
			});
		}
		else
		{
			var mimetype = mime.lookup(file)
			res.setHeader('Content-disposition', 'attachment; filename=' + filename);
			res.setHeader('Content-type', mimetype);
			var filestream = fs.createReadStream(file);
			filestream.pipe(res);
		}
	})
}