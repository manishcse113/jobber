/* Created by Manish */

var Device = require('../models/DeviceSchema.js');

// Create and Save 
exports.addDevice = (req, res) => {
	console.log("addDevice.controller create " + req.body);
    // Validate request
    if(!req.body.phone) {
        return res.status(400).send({
            message: "Device create -phone can not be empty"
        });
		
    }
	else if(!req.body.macId) {
        return res.status(400).send({
            message: "Device create -macId can not be empty"
        });
    }
	else if(!req.body.deviceName) {
        return res.status(400).send({
            message: "Device create -deviceName can not be empty"
        });
    }
	else if(!req.body.osVersion) {
        return res.status(400).send({
            message: "Device create -osVersion can not be empty"
        });
    }
	else if(!req.body.appSwVersion) {
        return res.status(400).send({
            message: "Device create -appSwVersion can not be empty"
        });
    }
	else if(!req.body.hwVersion) {
        return res.status(400).send({
            message: "Device create -hwVersion can not be empty"
        });
    }
	
	else
	{	
		Device.find(
			{
				$or: [	{macId: req.body.macId} ]
			}
			//,
			//{ _id: 1, phone: 1, city:1, location:1, userName:1, firstName:1, lastName:1 }
		)
		.then(result => {
			console.log("##########returned device###############" + result);
			if(result.length !== 0) {
					console.log("device already exists" + result);
					return res.status(404).send({
						message: "Device Already Exists MacId# "  + req.body.macId
					});
			}
			
			else
			{
				console.log("#################Device saving record ###################");
				var device = new Device();
				device.phone = req.body.phone;
				device.macId = req.body.macId;
				device.deviceName = req.body.deviceName;
				device.osVersion = req.body.osVersion;
				device.appSwVersion = req.body.appSwVersion;
				device.hwVersion = req.body.hwVersion;
				
				device.save(function(err, result2){
					if (err)
					{
						return res.send(err);
					}
					console.log("Success" + result2);
					res.send({
							message: "Success" ,
							result2
					});
				}).
				catch(err => {
					if(err.kind === 'ObjectId') {
						return res.status(404).send({
							message: "Error creating Device mac " + req.body.macId + "phone " + req.body.phone 
						});                
					}
					return res.status(500).send({
						message: "Error creating Device MacId " + req.body.macId + "phone " + req.body.phone
					});						
				});
			}
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Device could not be created with macId " + req.body.macId
				});                
			}
			return res.status(500).send({
				message: "Device could not be created with macId " + req.body.macId
			});
		});
		
	}
};


// get Device by macId
exports.getDevicebyMacId = (req, res) => {
	console.log("getDevice macId " + req);
    Device.find(
		{
			$and: [	{macId: req.params.macId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check deviceId "
				});
			}
			*/	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Device not found with macId " + req.params.macId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Device with macId " + req.params.macId
        });
    });
};


// get Device by phone
exports.getDevicebyPhone = (req, res) => {
	console.log("getDevice phone " + req);
    Device.find(
		{
			$and: [	{phone: req.params.phone}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check phone "
				});
			}
			*/	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Device not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Device with phone " + req.params.phone
        });
    });
};


// get All Devices List
exports.getAllDevices = (req, res) => {
	console.log("getDevices " + req);
    Device.find(
		{
			//$and: [	{userId: req.params.userId, status:req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId "
				});
			}
			*/
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Devices not found "
            });                
        }
        return res.status(500).send({
            message: "Devices could not be retrieved" 
        });
    });
};
