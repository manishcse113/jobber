var Consumer = require('../models/UserSchema.js');
// Create and Save 
exports.create = (req, res) => {
	console.log("user.controller create");
    // Validate request
    if(!req.body.phone) {
        return res.status(400).send({
            message: "user create -phone can not be empty"
        });
    }
	else if(!req.body.userName) {
        return res.status(400).send({
            message: "user create -userName can not be empty"
        });
    }
	else if(!req.body.password) {
        return res.status(400).send({
            message: "user create -password can not be empty"
        });
    }
		
	else
	{
		console.log("Checking if user already exists " + req.params);
		Consumer.find(
			{
				$or:[{userName:req.body.userName},{phone:req.body.phone}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				console.log(result);
				if(result.length !== 0) {
					console.log("user already exists" + consumer);
					return res.status(404).send({
						message: "User Already Exists - Choose new credentials or click forget Password" 
					});
				}
				else
				{
					console.log("user doesn't exists - creating new");
					//If everything ok, create a new record::
					console.log(req.body.userName);
					//Create a serviceman
					
					var consumer = new Consumer();
					consumer.userName = req.body.userName;
					consumer.password = req.body.password;
					consumer.phone = req.body.phone;
					consumer.email = '';
					consumer.state = '';
					consumer.city =  '';
					consumer.location = '';
					consumer.pin = '';
					consumer.firstName = '';
					consumer.lastName = '';
					consumer.photoUrl = '';
						
					consumer.save(function(err, result){
						if (err)
						{
							return res.send(err);
						}
						console.log("user create success" + result.phone);
						//res.send("user created");
						res.send({
								message: "user created succcess" ,
								userName: result.userName,
								_id: result._id
						});
						//message:'user created';
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating user " + req.params.userName
							});                
						}
						return res.status(500).send({
							message: "Error creating user " + req.params.userName
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "Consumer could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error Consumer could not be created"
				});
			});
	}
};

// login
exports.login = (req, res) => {
  console.log("login" + req);
    Consumer.find(
		{
			$and: [	{phone: req.params.phone},{password: req.params.password}]
		}
		,
		{ _id: 1, userName: 1 }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check login details for " + req.params.phone
				});
			}
			else{
				res.send({
					message:"login succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving user with phone " + req.params.phone
        });
    });
};

exports.forgetByName = (req, res) => {
    Consumer.find(
		{
			$and: [	{userName: req.params.userName} ]
		}
		,
		{ _id: 1, userName: 1 }
	)
    .then(consumer => {
		console.log(consumer);
        if(consumer.length === 0) {
            return res.status(404).send({
                message: "User not found- " + req.params.userName
            });            
        }
		else
		{
			res.send({
					message:"OTP sent",
					consumer
				});	
		}
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving User with phone " + req.params.phones
        });
    });
};

exports.forgetByPhone = (req, res) => {
 Consumer.find(
		{
			$and: [	{phone: req.params.phone} ]
		}
		,
		{ _id: 1, userName: 1 }
	)
    .then(consumer => {
        if(consumer.length === 0) {
            return res.status(404).send({
                message: "consumer not found with phone " + req.params.phone
            });            
        }
		else
		{
				return res.send({
					message:"OTP sent",
					consumer
				});
		}
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "consumer not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving consumer with phone " + req.params.phone
        });
    });
};

exports.resetPassword = (req, res) => {
	console.log("resetPassword");
	Consumer.find(
		{
			$and: [	{phone: req.params.phone}, {password:req.body.oldPassword} ]
		}
		,
		{ _id: 1, userName: 1 }
	)
    .then(consumer => {
        if(consumer.length === 0) {
            return res.status(404).send({
                message: "Consumer not found with phone or old password " + req.params.phone
            });            
        }
		else
		{
			// Find consumer and update it with the request body
			Consumer.findOneAndUpdate(
				{ "phone": req.params.phone },
				{ "$set":
					{
						password: req.body.newPassword
					}
				},
				{
					"fields": {"password":0},
					"new": true
				}
				)
				.then(consumer => {
					console.log("found consumer ", consumer);
					if(consumer.length===0) {
						return res.status(404).send({
							message: "consumer not found with phone " + req.params.phone
					});
				}
				return res.send({
							message:"Success",
							consumer
						});	
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "Consumer not found with phone " + req.params.phone
					});                
				}
				return res.status(500).send({
					message: "Error updating Consumer with phone " + req.params.phone
				});
			});
		}
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Consumer not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Consumer with phone " + req.params.phone
        });
    });
};


exports.findByCity = (req, res) => {
 Consumer.find(
		{
			$and: [	{city: req.params.city} ]
		}
		,
		{ password: 0 }
	)
    .then(consumers => {
		console.log("findbyCity #" + consumers);
		return res.send({
					message:"Success",
					consumers
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving consumers."
        });
    });
};


exports.findAllByLocation = (req, res) => {
	Consumer.find(
		{
			$and: [	{city: req.params.city}, {location: req.params.location} ]
		}
		,
		{ password: 0 }
	)
    .then(consumers => {
		return res.send({
					message:"Success",
					consumers
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving consumers."
        });
    });
};

exports.findAll = (req, res) => {
	Consumer.find(
			{},
			{ password: 0 }
		)
    .then(consumers => {
		return res.send({
					message:"Success",
					consumers
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving consumers."
        });
    });
};

exports.updateProfile = (req, res) => {
	// Validate Request
    if(!req.body.city) {
		console.log("not found with city");
        return res.status(400).send({
            message: "Consumer Profile city can not be empty"
        });
    }
    // Find Consumer and update it with the request body
    Consumer.findOneAndUpdate(
		{ "_id": req.params.consumerId },
		{ "$set":
			{
				state: req.body.state,
				city: req.body.city,
				location: req.body.location,
				pin: req.body.pin,
				email: req.body.email,
				firstName: req.body.firstName,
				lastName: req.body.lastName
			}
		},
		{
			"fields": {"password":0},
			"new": true
		}
		)
		.then(consumer => {
			console.log("found consumer ", consumer);
			if(consumer.length===0) {
				return res.status(404).send({
					message: "consumer not found with id " + req.params.consumerId
			});
        }
		return res.send({
					message:"Success",
					consumer
				});	
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Consumer not found with id " + req.params.consumerId
            });                
        }
        return res.status(500).send({
            message: "Error updating consumer with id " + req.params.consumerId
        });
    });
};

/*
exports.updateLike = (req, res) => {
	
	// Validate Request
    if(!req.body.like) {
        return res.status(400).send({
            message: "Consumer Profile like can not be empty"
        });
    }
	var flag=1;
	if (req.body.like==="true")
	{
		console.log("updateLike found true");
		Consumer.findOneAndUpdate(
			{_id:req.params.consumerId}, 
			{
				$inc: {	likeCount : 1}
			},
			{
				"fields": {"password":0},
				"new": true
			}
		)
		.then(consumer => {
			console.log(consumer);
			if(consumer.length===0) {
				return res.status(404).send({
					message: "consumer not found "
				});
			}
			return res.send({
					message:"Success",
					consumer
				});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Consumer not found with phone " + req.params.phone
				});                
			}
			return res.status(500).send({
				message: "Error updating consumer with phone " + req.params.phone
			});
		});	
	}
	else
	{
		flag = -1;
		Consumer.findOneAndUpdate(
			{_id:req.params.consumerId}, 
			{
				$inc: {	likeCount : -1}
			},
			{
				"fields": {"password":0},
				"new": true
			}
		)
		.then(consumer => {
			console.log(consumer);
			if(consumer.length===0) {
				return res.status(404).send({
					message: "consumer not found " + consumer
				});
			}
			return res.send({
					message:"Success",
					consumer
				});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Consumer not found with phone " + req.params.phone
				});                
			}
			return res.status(500).send({
				message: "Error updating consumer with phone " + req.params.phone
			});
		});	
	}	
};
*/