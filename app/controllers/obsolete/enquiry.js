var Favourite = require('../models/FavouriteSchema.js');
// Create and Save 
exports.addFavourite = (req, res) => {
	console.log("FavouriteMap.controller create");
    // Validate request
    if(!req.body.userId) {
        return res.status(400).send({
            message: "FavouriteMap create -userId can not be empty"
        });
    }
	
	else if(!req.body.smanId) {
        return res.status(400).send({
            message: "FavouriteMap create -smanId can not be empty"
        });
    }
	
	else
	{
		console.log("Checking if Favourite mapping already exists ");
		Favourite.find(
			{
				$and:[{userId:req.body.userId},{smanId:req.body.smanId}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				console.log(result);
				if(result.length !== 0) {
					console.log("Favourite Map already exists" + result);
					return res.status(404).send({
						message: "FavouriteMap Already Exists " 
					});
				}		
				else
				{
					console.log("Map doesn't exists - creating new");
					//If everything ok, create a new record::
					console.log(result);
					//Create a serviceman
					
					var favourite = new Favourite();
					favourite.userId = req.body.userId;
					favourite.smanId = req.body.smanId;
					
					favourite.save(function(err, result){
						if (err)
						{
							return res.send(err);
						}
						console.log("favouriteMap create success" + result);
						res.send({
								message: "favourite created success" ,
								userId: result.userId,
								smanId: result.smanId
						});
						//message:'user created';
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating favouriteMap " + req.body.userId + "map " + req.body.smanId
							});                
						}
						return res.status(500).send({
							message: "Error creating favouriteMap " + req.body.userId + "map " + req.body.smanId
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "favouriteMap could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error favouriteMap could not be created"
				});
			});
	}
};


// get Favourties
exports.getFavourite = (req, res) => {
  console.log("getFavourite userId " + req);
    Favourite.find(
		{
			$and: [	{userId: req.params.userId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId Id "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "map not found with userId " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Otp with phone " + req.params.userId
        });
    });
};

// Delete Favourite
exports.deleteFavourite = (req, res) => {
	console.log("deleteFavourite userId " + req);
    Favourite.find(
		{
			$and: [	{userId: req.params.userId}, {smanId: req.params.smanId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "not found "
				});
			}
			else{
				
				Favourite.findOneAndRemove(
					{'userId' : req.params.userId, 'smanId':req.params.smanId}, 
					function (err,result){
						if(err) throw err;
						else
						{
							res.send({
								message:"Success",
								result
							});			
						}
						//res.redirect('/newsfeed');
					}
				);
				/*
				res.send({
					message:"Success",
					result
				});
				*/
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "map not found with userId, smanId " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving userId, smanId " + req.params.userId
        });
    });
};

/*
var crypto = require('crypto');

function randomValueHex (len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
}
*/

//var value1 = randomValueHex(12) // value 'd5be8583137b'
//var value2 = randomValueHex(2)  // value 'd9'
