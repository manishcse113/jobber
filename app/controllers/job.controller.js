var Job = require('../models/JobSchema.js');
var Consumer = require('../models/UserSchema.js');
// Create and Save 
exports.addJob = (req, res) => {
	console.log("addJob.controller create " + req.body);
    // Validate request
    if(!req.body.userId) {
		
        return res.status(400).send({
            message: "Job create -userId can not be empty"
        });
		
    }
	else if(!req.body.city) {
        return res.status(400).send({
            message: "Job create -city can not be empty"
        });
    }
	else if(!req.body.location) {
        return res.status(400).send({
            message: "Job create -location can not be empty"
        });
    }
	else if(!req.body.phone) {
        return res.status(400).send({
            message: "Job create -phone can not be empty"
        });
    }
	else if(!req.body.serviceName) {
        return res.status(400).send({
            message: "Job create -serviceName can not be empty"
        });
    }
	else if(!req.body.title) {
        return res.status(400).send({
            message: "Job create -title can not be empty"
        });
    }
	/*
	else if(!req.body.description) {
        return res.status(400).send({
            message: "Job create -description can not be empty"
        });
    }*/
	else if(!req.body.date) {
        return res.status(400).send({
            message: "Job create -date can not be empty"
        });
    }
	else if(!req.body.time) {
        return res.status(400).send({
            message: "Job create -time can not be empty"
        });
    }
	else
	{	
		Consumer.find(
			{
				$or: [	{_id: req.body.userId} ]
			}
			//,
			//{ _id: 1, phone: 1, city:1, location:1, userName:1, firstName:1, lastName:1 }
		)
		.then(consumer => {
			console.log("##########returned consumer###############" + consumer);
			if(consumer.length === 0) {
				return res.status(404).send({
					message: "User not found- " + req.body.userId
				});            
			}
			else
			{
				
				console.log("#################Job saving record ###################");
				var job = new Job();
				job.userId = req.body.userId;
				job.userCity = req.body.city;
				job.userLocation = req.body.location;
				job.userPhone = req.body.phone;
				job.title = req.body.title;
				job.serviceName = req.body.serviceName;
				job.description = "";//req.body.description;	
				job.serviceDate = req.body.date;
				job.serviceTime = req.body.time;
				job.status = "Open";
				job.userFirstName = consumer[0].firstName;
				job.userLastName = consumer[0].lastName;
				job.userName = consumer[0].userName;
				job.save(function(err, result){
					if (err)
					{
						return res.send(err);
					}
					console.log("Success" + result);
					res.send({
							message: "Success" ,
							result
					});
				}).
				catch(err => {
					if(err.kind === 'ObjectId') {
						return res.status(404).send({
							message: "Error creating Job " + req.body.userId + "phone " + req.body.phone + 'jobTitle:' + title
						});                
					}
					return res.status(500).send({
						message: "Error creating Job " + req.body.userId + "phone " + req.body.phone + 'jobTitle:' + title
					});						
				});
			}
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "User not found with userId " + req.params.userId
				});                
			}
			return res.status(500).send({
				message: "Error retrieving User with userId " + req.params.userId
			});
		});
		
	}
};


// get All Jobs by me
exports.getJobsByMe = (req, res) => {
	console.log("getJobs userId " + req);
    Job.find(
		{
			$and: [	{userId: req.params.userId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId "
				});
			}
			*/	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with userId " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Jobs with userId " + req.params.userId
        });
    });
};


// get All Jobs by me
exports.getJobsByKeyWord = (req, res) => {
	console.log("getJobsByKeyWord userId " + req);
	
    Job.find(
		{ "$text": { "$search": req.params.keys } },
		{ "score": { "$meta": "textScore" } }
		
		/*,
		{
			$and: [	{userId: req.params.userId}]
		}
		*/
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with keys " + req.params.keys
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Jobs with keys " + req.params.keys
        });
    });
};



// get All Jobs by me
exports.getJobsByMeStatus = (req, res) => {
	console.log("getJobs userId " + req);
    Job.find(
		{
			$and: [	{userId: req.params.userId, status:req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId "
				});
			}
			*/
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with userId " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Jobs with userId " + req.params.userId
        });
    });
};


// get All Jobs by jobId
exports.getJobsByJobId = (req, res) => {
	console.log("getJobsByJobId " + req);
    Job.find(
		{
			$and: [	{_id: req.params.jobId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check jobId "
				});
			}
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with jobId " + req.params.jobId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Jobs with jobId " + req.params.jobId
        });
    });
};

// get Jobs by city, serviceName
exports.getJobsByCityLocationService = (req, res) => {
	console.log("getJobsByCityLocationService " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, userLocation: req.params.location, serviceName: req.params.serviceName}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city & servicename & location" + req.params.city + req.params.serviceName + req.params.userLocation
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city & servicename & location" + req.params.city + req.params.serviceName + req.params.userLocation
        });
    });
};


// get Jobs by city, serviceName
exports.getJobsByCityLocationServiceStatus = (req, res) => {
	console.log("getJobsByCityLocationServiceStatus " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, userLocation: req.params.location, serviceName: req.params.serviceName, status: req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city & servicename & location" + req.params.city + req.params.serviceName + req.params.userLocation
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city & servicename & location" + req.params.city + req.params.serviceName + req.params.userLocation
        });
    });
};

// get Jobs by city, location
exports.getJobsByCityLocation = (req, res) => {
	console.log("getJobsByCityLocation " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, userLocation: req.params.location}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);		
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city & location" + req.params.city + req.params.location
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city & location" + req.params.city + req.params.location
        });
    });
};

// get Jobs by city, location
exports.getJobsByCityLocationStatus = (req, res) => {
	console.log("getJobsByCityLocation " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, userLocation: req.params.location, status: req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);		
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city & location" + req.params.city + req.params.location
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city & location" + req.params.city + req.params.location
        });
    });
};


// get Jobs by city, serviceName
exports.getJobsByCityService = (req, res) => {
	console.log("getJobsByCityService " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, serviceName: req.params.serviceName}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);		
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city & servicename " + req.params.city + req.params.serviceName
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city & servicename " + req.params.city + req.params.serviceName
        });
    });
};

// get Jobs by city, serviceName
exports.getJobsByCityServiceStatus = (req, res) => {
	console.log("getJobsByCityService " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, serviceName: req.params.serviceName, status: req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);		
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city & servicename " + req.params.city + req.params.serviceName
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city & servicename " + req.params.city + req.params.serviceName
        });
    });
};

// get Jobs by city
exports.getJobsByCity = (req, res) => {
	console.log("getJobsByCity " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);		
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city" + req.params.city
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city " + req.params.city
        });
    });
};

exports.getJobsByCityStatus = (req, res) => {
	console.log("getJobsByCityStatus " + req);
    Job.find(
		{
			$and: [	{userCity: req.params.city, status: req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			else{
				console.log(result);		
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Jobs not found with city" + req.params.city
            });                
        }
        return res.status(500).send({
            message: "Jobs not found with city " + req.params.city
        });
    });
};


//Update a Job - based on JobID and other parameters:
exports.updateJob = (req, res) => {
	// Validate Request
    
    // Validate request
    if(!req.body.userId) {
        return res.status(400).send({
            message: "Job create -userId can not be empty"
        });
    }
	else if(!req.body.city) {
        return res.status(400).send({
            message: "Job create -city can not be empty"
        });
    }
	else if(!req.body.location) {
        return res.status(400).send({
            message: "Job create -location can not be empty"
        });
    }
	else if(!req.body.phone) {
        return res.status(400).send({
            message: "Job create -phone can not be empty"
        });
    }
	else if(!req.body.serviceName) {
        return res.status(400).send({
            message: "Job create -serviceName can not be empty"
        });
    }
	else if(!req.body.title) {
        return res.status(400).send({
            message: "Job create -title can not be empty"
        });
    }
	/*
	else if(!req.body.description) {
        return res.status(400).send({
            message: "Job create -description can not be empty"
        });
    }*/

	else
	{
		// Find job and update it with the request body
		Job.findOneAndUpdate(
			{ "_id": req.params.jobId },
			{ "$set":
				{
					userId:req.body.userId,
					userCity : req.body.city,
					userLocation : req.body.location,
					userPhone : req.body.phone,
					title : req.body.title,
					serviceName : req.body.serviceName,
					description : req.body.description,	
					status : req.body.status
					//job.userFirstName : consumer.firstName;
					//job.userLastName:consumer.lastName;
					//job.userName : consumer.userName;
				}
			},
			{
				"new": true
			}
			)
			.then(job => {
				console.log("found job ", job);
				if(job.length===0) {
					return res.status(404).send({
						message: "job not found with id " + req.params._id
				});
			}
			return res.send({
						message:"Success",
						job
					});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "job not found with id " + req.params._id
				});                
			}
			return res.status(500).send({
				message: "Error updating job with id " + req.params._id
			});
		});
	}
};

//###############