var Candidate = require('../models/CandidateSchema.js');

// Create and Save 
exports.create = (req, res) => {
	console.log("candidate.controller create");
    // Validate request
    if(!req.body.phone) {
        return res.status(400).send({
            message: "candidate create -phone can not be empty"
        });
    }
	else
	{
		Candidate.find(
			{
				$or:[{phone:req.body.phone}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				
				else
				{
					//Create a Candidate
					var candidate = new Candidate();
					candidate.phone = req.body.phone;
					
					if (!req.body.email) {
						candidate.email =  '';
					} else {
						candidate.email =  req.body.email;
					}
					if (!req.body.firstName) {
						candidate.firstName =  '';
					} else {
						candidate.firstName =  req.body.firstName;
					}
					
					if (!req.body.lastName) {
						candidate.lastName =  '';
					} else {
						candidate.lastName =  req.body.lastName;
					}
					
					if (!req.body.pin) {
						candidate.pin =  '';
					} else {
						candidate.pin =  req.body.pin;
					}
					
					if (!req.body.skills) {
						candidate.skills =  '';
					} else {
						candidate.skills =  req.body.skills;
					}
					
					if (!req.body.title) {
						candidate.title =  '';
					} else {
						candidate.title =  req.body.title;
					}
					
					if (!req.body.description) {
						candidate.description =  '';
					} else {
						candidate.description =  req.body.description;
					}
					
					if (!req.body.state) {
						candidate.state =  '';
					} else {
						candidate.state =  req.body.state;
					}
									
					if (!req.body.city) {
						candidate.city =  '';
					} else {
						candidate.city =  req.body.city;
					}
					
					if(!req.body.location) {
						candidate.location = '';
					} else {
						candidate.location = req.body.location;
					}
					
					if(!req.body.photoUrl) {
						candidate.photoUrl = '';
					} else {
						candidate.photoUrl = req.body.photoUrl;
					}
						
					candidate.save(function(err, result){
						if (err) {
							return res.send(err);
						}
						console.log("candidate create success" + result.phone);
						res.send({
								message: "success" 
						});
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating candidate record "
							});                
						}
						return res.status(500).send({
							message: "Error creating candidate record "
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "Candidate could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error Candidate could not be created"
				});
			});
	}
};

exports.findAll = (req, res) => {
 Candidate.find(
	)
    .then(candidates => {
		return res.send({
					message:"Success",
					candidates
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving candidates."
        });
    });
};

exports.updateProfile = (req, res) => {
	// Validate Request
    if(!req.body.phone) {
		console.log("not found with phone");
        return res.status(400).send({
            message: "Candidate Profile phone can not be empty"
        });
    }
    // Find Candidate and update it with the request body
    Candidate.findOneAndUpdate(
		{ "phone": req.params.phone },
		{ "$set":
			{
				state: req.body.state,
				city: req.body.city,
				location: req.body.location,
				pin: req.body.pin,
				email: req.body.email,
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				title: req.body.title,
				description: req.body.description,
				skills: req.body.skills,
				photoUrl: req.body.photoUrl,
			}
		}
		)
		.then(candidate => {
			console.log("found candidate ", candidate);
			if(candidate.length===0) {
				return res.status(404).send({
					message: "candidate not found with phone " + req.params.phone
			});
        }
		return res.send({
					message:"Success",
					candidate
				});
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Candidate not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error updating consumer with phone " + req.params.phone
        });
    });
};

exports.updateProfilePic = (req, res) => {
    // Find Candidate and update it with the request body
    Candidate.findOneAndUpdate(
		{ "phone": req.params.candidate },
		{ "$set":
			{
				photoUrl: req.file.originalname
			}
		},
		{
			"new": true
		}
		)
		.then(candidate => {
			console.log("found candidate ", candidate);
			if(candidate.length===0) {
				return res.status(404).send({
					message: "candidate not found with phone " + req.params.phone
			});
        }
		return res.send({
					message:"Success",
					candidate
				});	
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Candidate not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error updating consumer with phone " + req.params.phone
        });
    });
};