var Response = require('../models/JobResponseSchema.js');
var Consumer = require('../models/UserSchema.js');
// Create and Save 
exports.addResponse = (req, res) => {
	console.log("addResponse.controller create");
    // Validate request
	//JobId is used for parent Id : which can be either JobId or first MessageId
	//boolean isJobId = true;
    if(!req.body.jobId) {
		//isJobId = false;
			return res.status(400).send({
				message: "Response create -one of the jobId or messageId is must"
			});
    }
	else if(!req.body.senderId) {
        return res.status(400).send({
            message: "Response create -senderId can not be empty"
        });
    }
	else if(!req.body.senderUserName) {
        return res.status(400).send({
            message: "Response create -senderUserName can not be empty"
        });
    }
	else if(!req.body.senderPhone) {
        return res.status(400).send({
            message: "Response create -senderPhone can not be empty"
        });
    }
	else if(!req.body.isConsumer) {
        return res.status(400).send({
            message: "Response create -isConsumer can not be empty"
        });
    }
	else if(!req.body.msg) {
        return res.status(400).send({
            message: "Response create -msg can not be empty"
        });
    }
	else
	{			
		console.log("Job Comment saving record ");
		var response = new Response();
		
		
		response.jobId = req.body.jobId;
		response.senderId = req.body.senderId;
		response.senderUserName = req.body.senderUserName;
		response.senderPhone = req.body.senderPhone;
		response.senderFirstName = req.body.senderFirstName;
		response.msg = req.body.msg;
		response.parentResponseId = req.body.parentResponseId;
		response.isConsumer = req.body.isConsumer;		
		response.save(function(err, result){
			if (err)
			{
				return res.send(err);
			}
			console.log("Response create success" + result);
			res.send({
					message: "success" ,
					result
			});
		}).
		catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Error creating Response +JobID " + req.body.jobId + 'msg:' + req.body.msg
				});                
			}
			return res.status(500).send({
				message: "Error creating Response +JobID " + req.body.jobId + 'msg:' + req.body.msg
			});						
		});
	}
};


// get Responses by me
exports.getResponseByMe = (req, res) => {
	console.log("getResponse senderId " + req);
    Response.find(
		{
			$and: [	{senderId: req.params.senderId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check senderId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Reseponse not found with senderId " + req.params.senderId
            });                
        }
        return res.status(500).send({
            message: "Reseponse not found with senderId " + req.params.senderId
        });
    });
};


// get Responses by JobId
exports.getResponseByJob = (req, res) => {
	console.log("getResponse jobId " + req);
    Response.find(
		{
			$and: [	{jobId: req.params.jobId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check jobId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(
			404).send({
                message: "Reseponse not found with jobId " + req.params.jobId
            });                
        }
        return res.status(500).send({
            message: "Reseponse not found with jobId " + req.params.jobId
        });
    });
};

// get Responses by responseId
exports.getChildResponses = (req, res) => {
	console.log("getResponse responseId " + req);
    Response.find(
		{
			$and: [	{parentResponseId: req.params.responseId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check parentResponseId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Reseponse not found with responseId " + req.params.responseId
            });                
        }
        return res.status(500).send({
            message: "Reseponse not found with jobId " + req.params.jobId
        });
    });
};