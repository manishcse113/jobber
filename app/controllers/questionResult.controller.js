var QuestionResult = require('../models/QuestionResultSchema.js');
// Create and Save 


const translate = require('google-translate-api');
 
//For dialogflow 
var args = {    
	headers: { 
		"Authorization": 
		"Bearer c49b5522c5b14de88dd83288d04e6274"
		//"Bearer cd160d73b9034544845010311e399217" 
	}
};


// You can find your project ID in your Dialogflow agent settings
const projectId = 'langaugetest-e9160'; //https://dialogflow.com/docs/agents#settings
const sessionId = 'quickstart-session-id';
const query = 'hello';
const languageCode = 'en-US';
// Instantiate a DialogFlow client.
const dialogflow = require('dialogflow');
const sessionClient = new dialogflow.SessionsClient();
// Define session path
const sessionPath = sessionClient.sessionPath(projectId, sessionId);

exports.create = (req, res) => {
	console.log("QuestionResult.controller create");
    // Validate request
    if(!req.body.userId) {
        return res.status(400).send({
            message: "QuestionResult create -userId can not be empty"
        });
    }
	
	else if(!req.body.testId) {
        return res.status(400).send({
            message: "QuestionResult create -testId can not be empty"
        });
    }
	else if(!req.body.qId) {
        return res.status(400).send({
            message: "QuestionResult create -qId can not be empty"
        });
    }
	else if(!req.body.testLanguage) {
        return res.status(400).send({
            message: "QuestionResult create -testLanguage can not be empty"
        });
    }
	else if(!req.body.question) {
        return res.status(400).send({
            message: "QuestionResult create -question can not be empty"
        });
    }
		
	else if(!req.body.ut) {
        return res.status(400).send({
            message: "QuestionResult create -ut can not be empty"
        });
    }
		
	else if(!req.body.userTestNumber) {
        return res.status(400).send({
            message: "QuestionResult create -userTestNumber can not be empty"
        });
    }
		
	else
	{
		
		
		//Here we have to get the intent 1> for the question, 2> for the ut->gt and then check intent. If both intent are same with response text -'Ok', then we can say translation is correct.
		//Step1: Detect question intent
		//Step2: Detect translation intent
		// After getting both results only declare the result:
		
		//Here is step1
		var intentDetected = 0;
		var resultMatched = 0;
		var marks = 0; 
		var questionIntent='';
		var translationIntent='';
		const request = {
			  session: sessionPath,
			  queryInput: {
				text: {
				  text: req.body.question, //This is for question
				  languageCode: 'en',
				},
			  },
			};
		sessionClient.detectIntent(request)
			.then(responses => {
				console.log('Detected intent');
				const result = responses[0].queryResult;
				console.log(`  Query: ${result.queryText}`);
				console.log(`  Response: ${result.fulfillmentText}`);
				if (result.intent) {
					console.log(`  Intent: ${result.intent.displayName}`);
					intentDetected++;
					questionIntent = result.intent.displayName
					if (result.fulfillmentText==='Ok')
					{
						resultMatched++;
						if (questionIntent==translationIntent)
						{
							marks=1;
						}
					}
				} 
				else {
				  console.log(`  No intent matched.`);

				}
				if (intentDetected==2)
				{
					console.log("Checking if it already exists " + req.params);
					QuestionResult.find(
					{
						$and:[{userId:req.body.userId}, {testId:req.body.testId}, {qId:req.body.qId}, {userTestNumber:req.body.userTestNumber}]
					}
					, 
					function (err, result) 
					{
						if (err) throw err;
						console.log(result);
						if(result.length !== 0) {
							console.log("result already exists" );
						}
						console.log("result doesn't exists - creating new");
						//If everything ok, create a new record::
						console.log(req.body.testId);
						//Create a serviceman
						
						var qresult = new QuestionResult();
						qresult.testId = req.body.testId;
						qresult.userId = req.body.userId;
						qresult.qId = req.body.qId;
						qresult.testLanguage = req.body.testLanguage;
						qresult.question = req.body.question;
						qresult.ut = req.body.ut;
						qresult.gt = translatedEnglish;
						qresult.marks = marks;
						qresult.userTestNumber = req.body.userTestNumber;
						qresult.save(function(err, result){
							if (err)
							{
								return res.send(err);
							}
							console.log("qresult create success" + result);
							//res.send("user created");
							res.send({
									message: "test created succcess" ,
									result: qresult
							});
							//message:'user created';
						}).
						catch(err => {
							if(err.kind === 'ObjectId') {
								return res.status(404).send({
									message: "Error creating result " + req.params.testId
								});                
							}
							return res.status(500).send({
								message: "Error creating result " + req.params.testId
							});
						});
						
					}).catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "result could not be created"
							});                
						}
						return res.status(500).send({
							message: "Error result could not be created"
						});
					}); 
				}
			})
		.catch(err => {
			console.error('ERROR:', err);
		});
			
		//Here is step2
		var translatedEnglish;
		translate( req.body.ut, {from:req.body.testLanguage,to: 'en'}).then(response => 
		{
			translatedEnglish = response.text;
			console.log("translated####" + translatedEnglish);			
			// The text query request.
			const request = {
			  session: sessionPath,
			  queryInput: {
				text: {
				  text: translatedEnglish, //This is for response from user
				  languageCode: 'en',
				},
			  },
			};
			
			// Send request and log result
			sessionClient.detectIntent(request)
			.then(responses => {
				console.log('Detected intent');
				const result = responses[0].queryResult;
				console.log(`  Query: ${result.queryText}`);
				console.log(`  Response: ${result.fulfillmentText}`);
				if (result.intent) {
					console.log(`  Intent: ${result.intent.displayName}`);
					intentDetected++;
					translationIntent = result.intent.displayName
					if (result.fulfillmentText==='Ok')
					{
						resultMatched++;
						if (questionIntent==translationIntent)
						{
							marks=1;
						}
					}
				} 
				else {
				  console.log(`  No intent matched.`);

				}
				if (intentDetected==2)
				{
					console.log("Checking if it already exists " + req.params);
					QuestionResult.find(
					{
						$and:[{userId:req.body.userId}, {testId:req.body.testId}, {qId:req.body.qId}, {userTestNumber:req.body.userTestNumber}]
					}
					, 
					function (err, result) 
					{
						if (err) throw err;
						console.log(result);
						if(result.length !== 0) {
							console.log("result already exists" );
						}
						console.log("result doesn't exists - creating new");
						//If everything ok, create a new record::
						console.log(req.body.testId);
						//Create a serviceman
						
						var qresult = new QuestionResult();
						qresult.testId = req.body.testId;
						qresult.userId = req.body.userId;
						qresult.qId = req.body.qId;
						qresult.testLanguage = req.body.testLanguage;
						qresult.question = req.body.question;
						qresult.ut = req.body.ut;
						qresult.gt = translatedEnglish;
						qresult.marks = marks;
						qresult.userTestNumber = req.body.userTestNumber;
						qresult.save(function(err, result){
							if (err)
							{
								return res.send(err);
							}
							console.log("qresult create success" + result);
							//res.send("user created");
							res.send({
									message: "test created succcess" ,
									result: qresult
							});
							//message:'user created';
						}).
						catch(err => {
							if(err.kind === 'ObjectId') {
								return res.status(404).send({
									message: "Error creating result " + req.params.testId
								});                
							}
							return res.status(500).send({
								message: "Error creating result " + req.params.testId
							});
						});
						
					}).catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "result could not be created"
							});                
						}
						return res.status(500).send({
							message: "Error result could not be created"
						});
					}); 
				}
				
			})
			.catch(err => {
				console.error('ERROR:', err);
			});
			
		}).catch(err => {
			console.error(err);
		})
	}
};


exports.getByUserTestNumber = (req, res) => {
	console.log("getByUserTestNumber" + req);
	
    QuestionResult.find(
		{
			$and: [	{testId: req.params.testId}, {userId: req.params.userId}, {userTestNumber: req.params.userTestNumber}]
		}
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check details for testId " + req.params.testId
				});
			}
			else{
				res.send({
					message:"Result get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Result not found with testId " + req.params.testId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Result " + req.params.testId
        });
    });
};


exports.getByUserTestId = (req, res) => {
	console.log("getByUserTestId" + req);
	
    QuestionResult.find(
		{
			$and: [	{testId: req.params.testId}, {userId: req.params.userId}]
		}
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check details for testId " + req.params.testId
				});
			}
			else{
				res.send({
					message:"Result get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Result not found with testId " + req.params.testId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Result " + req.params.testId
        });
    });
};

exports.getByUserId = (req, res) => {
	console.log("getByUserId" + req);
	
    QuestionResult.find(
		{
			$and: [	{userId: req.params.userId}]
		}
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check details for userId " + req.params.userId
				});
			}
			else{
				res.send({
					message:"Result get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Result not found with testId " + req.params.testId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Result " + req.params.testId
        });
    });
};

exports.getAll = (req, res) => {
	console.log("getAll" + req);
	
    QuestionResult.find(
		{
		}
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "No record found for result "
				});
			}
			else{
				res.send({
					message:"Result get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Result not found " 
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Result "
        });
    });
};


//Update a QResult - based on questionResultId, gt, marks, remarks
exports.updateQResult = (req, res) => {
	// Validate Request
    
    // Validate request
	if(!req.body._id) {
        return res.status(400).send({
            message: "updateQResult -_id can not be empty"
        });
    }
	
    if(!req.body.gt) {
        return res.status(400).send({
            message: "updateQResult -gt can not be empty"
        });
    }
	
	else if(!req.body.marks) {
        return res.status(400).send({
            message: "updateQResult -marks can not be empty"
        });
    }
	
	else
	{
		// Find qResult and update it with the request body
		QuestionResult.findOneAndUpdate(
			{ "_id": req.body._id },
			{ "$set":
				{
					gt:req.body.gt,
					marks : req.body.marks
				}
			},
			{
				"new": true
			}
			)
			.then(qResult => {
				console.log("found qResult ", qResult);
				if(qResult.length===0) {
					return res.status(404).send({
						message: "qResult not found with id " + req.body._id
				});
			}
			return res.send({
						message:"Success",
						qResult
					});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "qResult not found with id " + req.body._id
				});                
			}
			return res.status(500).send({
				message: "Error updating qResult with id " + req.body._id
			});
		});
	}
};

//Update a QResult - based on questionResultId, gt
exports.updateQResultGt = (req, res) => {
	// Validate Request
    
    // Validate request
	if(!req.body._id) {
        return res.status(400).send({
            message: "updateQResult -_id can not be empty"
        });
    }
	
    if(!req.body.gt) {
        return res.status(400).send({
            message: "updateQResult -gt can not be empty"
        });
    }
	
	
	else
	{
		// Find qResult and update it with the request body
		QuestionResult.findOneAndUpdate(
			{ "_id": req.body._id },
			{ "$set":
				{
					gt:req.body.gt
				}
			},
			{
				"new": true
			}
			)
			.then(qResult => {
				console.log("found qResult ", qResult);
				if(qResult.length===0) {
					return res.status(404).send({
						message: "qResult not found with id " + req.body._id
				});
			}
			return res.send({
						message:"Success",
						qResult
					});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "qResult not found with id " + req.body._id
				});                
			}
			return res.status(500).send({
				message: "Error updating qResult with id " + req.body._id
			});
		});
	}
};

//Update a QResult - based on questionResultId, Marks
exports.updateQResultMarks = (req, res) => {
	// Validate Request
    
    // Validate request
	if(!req.body._id) {
        return res.status(400).send({
            message: "updateQResult -_id can not be empty"
        });
    }
	
    if(!req.body.marks) {
        return res.status(400).send({
            message: "updateQResult -marks can not be empty"
        });
    }
	
	
	else
	{
		// Find qResult and update it with the request body
		QuestionResult.findOneAndUpdate(
			{ "_id": req.body._id },
			{ "$set":
				{
					marks:req.body.marks
				}
			},
			{
				"new": true
			}
			)
			.then(qResult => {
				console.log("found qResult ", qResult);
				if(qResult.length===0) {
					return res.status(404).send({
						message: "qResult not found with id " + req.body._id
				});
			}
			return res.send({
						message:"Success",
						qResult
					});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "qResult not found with id " + req.body._id
				});                
			}
			return res.status(500).send({
				message: "Error updating qResult with id " + req.body._id
			});
		});
	}
};

