/* Created by Manish */

var DeviceLastEvent = require('../models/DeviceLastEventsSchema.js');

// Create and Save 
exports.addDeviceEvent = (req, res) => {
	console.log("addDeviceLastEvent.controller create " + req.body);
    // Validate request
    if(!req.body.macId) {
        return res.status(400).send({
            message: "Device Event create -macId can not be empty"
        });
    }
	else if(!req.body.eventId) {
        return res.status(400).send({
            message: "Device Event create -eventId can not be empty"
        });
    }
	else if(!req.body.date) {
        return res.status(400).send({
            message: "Device create -date can not be empty"
        });
    }
	else if(!req.body.month) {
        return res.status(400).send({
            message: "Device Event create -month can not be empty"
        });
    }
	else if(!req.body.year) {
        return res.status(400).send({
            message: "Device Event create -year can not be empty"
        });
    }
	else if(!req.body.hour) {
        return res.status(400).send({
            message: "Device Event create -hour can not be empty"
        });
    }
	else if(!req.body.minute) {
        return res.status(400).send({
            message: "Device Event create -minute can not be empty"
        });
    }
	else if(!req.body.second) {
        return res.status(400).send({
            message: "Device Event create -second can not be empty"
        });
    }
	else if(!req.body.eventId) {
        return res.status(400).send({
            message: "Device Event create -eventId can not be empty"
        });
    }
	
	
	else
	{	
		console.log("#################DeviceEvent saving record ###################");
		var deviceEvent = new DeviceLastEvent();

		deviceEvent.macId = req.body.macId;
		deviceEvent.eventId = req.body.eventId;
		
		deviceEvent.date = req.body.date;
		deviceEvent.month = req.body.month;
		deviceEvent.year = req.body.year;
		deviceEvent.hour = req.body.hour;
		deviceEvent.minute = req.body.minute;
		deviceEvent.second = req.body.second;
		
		deviceEvent.save(function(err, result2){
			if (err)
			{
				return res.send(err);
			}
			console.log("Success" + result2);
			res.send({
					message: "Success" ,
					result2
			});
		}).
		catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Error creating deviceEvent mac " + req.body.macId 
				});                
			}
			return res.status(500).send({
				message: "Error creating deviceEvent MacId " + req.body.macId 
			});						
		});
	}
};


// get Device by macId
exports.getDeviceEventsbyMacId = (req, res) => {
	console.log("getDeviceEvent macId " + req);
    DeviceLastEvent.find(
		{
			$and: [	{macId: req.params.macId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check deviceId "
				});
			}
			*/	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeviceEvents not found with macId " + req.params.macId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving DeviceEvents with macId " + req.params.macId
        });
    });
};



// get All Events List
exports.getAllEvents = (req, res) => {
	console.log("getDevicesEvents " + req);
    DeviceLastEvent.find(
		{
			//$and: [	{userId: req.params.userId, status:req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId "
				});
			}
			*/
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeviceEvent not found "
            });                
        }
        return res.status(500).send({
            message: "DeviceEvent could not be retrieved" 
        });
    });
};
