var Enquiry = require('../models/EnquirySchema.js');
var Consumer = require('../models/UserSchema.js');
var Sman = require('../models/ServiceManSchema.js');
var OtpController = require('./otp.controller.js');


// Create and Save 
exports.addEnquiry = (req, res) => {
	console.log("addEnquiry.controller create");
    // Validate request
    if(!req.body.userId) {
        return res.status(400).send({
            message: "Enquiry create -userId can not be empty"
        });
    }
	else if(!req.body.smanId) {
        return res.status(400).send({
            message: "Enquiry create -smanId can not be empty"
        });
    }
	else if(!req.body.msg) {
        return res.status(400).send({
            message: "Enquiry create -msg can not be empty"
        });
    }
	else
	{
			
		Consumer.find(
			{
				$and: [	{_id: req.body.userId} ]
			}
			//,
			//{ _id: 1, phone: 1, city:1, location:1 }
		)
		.then(consumer => {
			console.log("####Consumers####" + consumer);
			if(consumer.length === 0) {
				return res.status(404).send({
					message: "User not found- " + req.body.userId
				});            
			}
			else
			{
				console.log("Enquiry saving record ");
				var enquiry = new Enquiry();
				enquiry.userId = req.body.userId;
				enquiry.serviceManId = req.body.smanId;
				enquiry.msg = req.body.msg;
				enquiry.userCity = consumer[0].city;
				enquiry.userLocation = consumer[0].location;
				enquiry.userPhone = consumer[0].phone;					
				enquiry.save(function(err, result){
					if (err)
					{
						return res.send(err);
					}
					console.log("Enquiry create success" + result);
					
					//send sms as well to serviceman::
					Sman.find(
						{
							$and: [	{_id: req.body.smanId} ]
						}
						//,
						//{ _id: 1, phone: 1, city:1, location:1 }
					)
					.then(sman => 
					{
						console.log("####sman####" + sman);
						var message = "Jobber Enquiry from " + consumer[0].userName + " " + consumer[0].phone +" Msg#" + req.body.msg; 
						console.log("####sman####" + message);
						OtpController.sendMessage(message, sman[0].phone);
					});
					
					res.send({
							message: "Success" ,
							result
					});
				}).
				catch(err => {
					if(err.kind === 'ObjectId') {
						return res.status(404).send({
							message: "Error creating Enquiry " + req.body.userId + "sman " + req.body.smanId + 'msg:' + req.body.msg
						});                
					}
					return res.status(500).send({
						message: "Error creating Enquiry " + req.body.userId + "map " + req.body.smanId + 'msg:' + req.body.msg
					});						
				});
				
			}
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "User not found with userId " + req.params.userId
				});                
			}
			return res.status(500).send({
				message: "Error retrieving User with userId " + req.params.userId
			});
		});
	}
};


// get Enquiries by me
exports.getEnquiryByMe = (req, res) => {
	console.log("getEnquiry userId " + req);
	
	
	Enquiry.aggregate(
		[{ $match: {userId: req.params.userId}},
		{ $lookup: {from: "servicemen", localField: "serviceManId", foreignField: "_id", as: "SMan"} }
		,
		{ $unwind: "$SMan"}
		]
		 
		, 
		//{ "$unwind": "$SMan" },
		function (err, result) {
			if (err) {  //next(err);
				console.log("Error in aggregation ##" + err);
				res.send({ message:"Failure", err });
			}
			else
			{
				res.send({ message:"Success", result });
			}
		}
	);
  
  
	/*
    Enquiry.find(  { $and: [	{userId: req.params.userId}] }).
		populate(
			{
				path:'sman',
				match:{_id:serviceManId}
			}
		).
		exec (function (err, result) {
			if (err) return handleError(err);
			//console.log('The author is %s', result.sman.phone);
			res.send({
						message:"Success",
						result
					});
			// prints "The author is Ian Fleming"
		});
	*/	
		/*
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId "
				});
			}
			else{					
				Sman.aggregate([
				{ $lookup:
				   {
					 from: 'result',
					 localField: '_id',
					 foreignField: 'serviceManId',
					 as: 'smanDetails'
				   }
				 }
				])=>(err, result2) {
					if (err) throw err;
					console.log(JSON.stringify(result2));
					//db.close();
					res.send({
						message:"Success",
						result2
					});
				});	
				
			}
        }
		
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Enquiry not found with userId " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Enquiry with userId " + req.params.userId
        });
    });
	*/
};


// get Enquiries for serviceman
exports.getEnquiryForSMan = (req, res) => {
  console.log("getEnquiry sManId " + req);
    Enquiry.find(
		{
			$and: [	{serviceManId: req.params.sManId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check sManId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Enquiry not found for sManId " + req.params.sManId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Enquiry for sManId " + req.params.sManId
        });
    });
};