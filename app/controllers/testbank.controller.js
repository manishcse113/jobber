var TestBank = require('../models/TestBankSchema.js');
// Create and Save 
exports.create = (req, res) => {
	console.log("user.controller create");
    // Validate request
    if(!req.body.testName) {
        return res.status(400).send({
            message: "testBank create -testName can not be empty"
        });
    }
	
	else if(!req.body.q1) {
        return res.status(400).send({
            message: "testBank create -q1 can not be empty"
        });
    }
	else if(!req.body.q2) {
        return res.status(400).send({
            message: "testBank create -q2 can not be empty"
        });
    }
	else if(!req.body.q3) {
        return res.status(400).send({
            message: "testBank create -q3 can not be empty"
        });
    }
	else if(!req.body.q4) {
        return res.status(400).send({
            message: "testBank create -q4 can not be empty"
        });
    }
	else if(!req.body.q5) {
        return res.status(400).send({
            message: "testBank create -q5 can not be empty"
        });
    }
	else if(!req.body.q6) {
        return res.status(400).send({
            message: "testBank create -q6 can not be empty"
        });
    }
	else if(!req.body.q7) {
        return res.status(400).send({
            message: "testBank create -q7 can not be empty"
        });
    }
	else if(!req.body.q8) {
        return res.status(400).send({
            message: "testBank create -q8 can not be empty"
        });
    }
	else if(!req.body.q9) {
        return res.status(400).send({
            message: "testBank create -q9 can not be empty"
        });
    }
	else if(!req.body.q10) {
        return res.status(400).send({
            message: "testBank create -q10 can not be empty"
        });
    }
		
	else
	{
		console.log("Checking if test already exists " + req.params);
		TestBank.find(
			{
				$or:[{testName:req.body.testName}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				console.log(result);
				if(result.length !== 0) {
					console.log("test already exists" + result);
					return res.status(404).send({
						message: "Test Already Exists - Choose new test name" 
					});
				}
				else
				{
					console.log("test doesn't exists - creating new");
					//If everything ok, create a new record::
					console.log(req.body.testName);
					//Create a serviceman
					
					var test = new TestBank();
					test.testName = req.body.testName;
					test.q1 = req.body.q1;
					test.q2 = req.body.q2;
					test.q3 = req.body.q3;
					test.q4 = req.body.q4;
					test.q5 = req.body.q5;
					test.q6 = req.body.q6;
					test.q7 = req.body.q7;
					test.q8 = req.body.q8;
					test.q9 = req.body.q9;
					test.q10 = req.body.q10;
						
					test.save(function(err, result){
						if (err)
						{
							return res.send(err);
						}
						console.log("test create success" + result.testName);
						//res.send("user created");
						res.send({
								message: "test created succcess" ,
								result: result
						});
						//message:'user created';
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating test " + req.params.testName
							});                
						}
						return res.status(500).send({
							message: "Error creating test " + req.params.testName
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "Test could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error Test could not be created"
				});
			});
	}
};

// getRandom
exports.getRandom = (req, res) => {
	console.log("getRandom" + req);
	
	var recordCount = TestBank.find().count()
	console.log("recordCount##" + recordCount);
    var random = randomValueHex(recordCount)
	console.log("random##" + random);
	TestBank.findOne({}, {skip:1}
	
	/*
	)
    TestBank.find(
		{
			$and: [	{phone: req.params.phone},{password: req.params.password}]
		}
		,
		{ _id: 1, userName: 1 }
	*/
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check details for random "
				});
			}
			else{
				res.send({
					message:"test get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Test not found with random "
            });                
        }
        return res.status(500).send({
            message: "Error retrieving test "
        });
    });
};


const translate = require('google-translate-api');
 

exports.getByName = (req, res) => {
	console.log("getByName" + req);
	
    TestBank.find(
		{
			$and: [	{testName: req.params.testName}]
		}
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check details for testName " + req.params.testName
				});
			}
			else{
				var translationCount=0;
				
				//console.log(result[0].q1);
				translate(result[0].q1, {from: 'en', to: req.params.language}).then(res => {
					result[0].t1 = res.text;
					console.log("translation t1 is " + res.text);
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}					
				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q2, {from: 'en', to: req.params.language}).then(res => {
						result[0].t2 = res.text;
						translationCount++;
						if (translationCount==10)
						{
							res.send({
								message:"test get succuess",
								result
							})	
						}
						
				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q3, {from: 'en', to: req.params.language}).then(res => {
					result[0].t3 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}

				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q4, {from: 'en', to: req.params.language}).then(res => {
					result[0].t4 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}

				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q5, {from: 'en', to: req.params.language}).then(res => {
					result[0].t5 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}

				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q6, {from: 'en', to: req.params.language}).then(res => {
					result[0].t6 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q7, {from: 'en', to: req.params.language}).then(res => {
					result[0].t7 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q8, {from: 'en', to: req.params.language}).then(res => {
					result[0].t8 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
				}).catch(err => {
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
					console.error(err);
				})
				translate(result[0].q9, {from: 'en', to: req.params.language}).then(res => {
					result[0].t9 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
				
				}).catch(err => {
					console.error(err);
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
				})				
				translate(result[0].q10, {from: 'en', to: req.params.language}).then(res => {
					result[0].t10 = res.text;
					translationCount++;
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}						
				}).catch(err => {
					console.error(err);
					if (translationCount==10)
					{
						res.send({
							message:"test get succuess",
							result
						})	
					}
				});
	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Test not found with testName " + req.params.testName
            });                
        }
        return res.status(500).send({
            message: "Error retrieving test " + req.params.testName
        });
    });
};

function translate2(input, language)
{
	translate(input, {from: 'en', to: language}).then(res => {
					return res.text;
	}).catch(err => {
		console.error(err);
	})
}
				

exports.getAll = (req, res) => {
	console.log("getAll" + req);
	
    TestBank.find(
		{
		}
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "No record found for test "
				});
			}
			else{
				res.send({
					message:"test get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Test not found " 
            });                
        }
        return res.status(500).send({
            message: "Error retrieving test "
        });
    });
};

exports.getAllNames = (req, res) => {
	console.log("getAll" + req);
	
    TestBank.find(
		{
		}
		,
		{ testName: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "No record found for test "
				});
			}
			else{
				res.send({
					message:"test get succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Test not found " 
            });                
        }
        return res.status(500).send({
            message: "Error retrieving test "
        });
    });
};


var crypto = require('crypto');
function randomValueHex (len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
}




//AIzaSyAqQxf_SY4MzjuT2eZY2NJ1j3qo3kXbtwA



/*
// Imports the Google Cloud client library
const Translate = require('@google-cloud/translate');
// Your Google Cloud Platform project ID
const projectId = 'translate-1-191d5';
// Instantiates a client
const translate = new Translate({
  projectId: projectId,
});

function translateTo(textToTranslate, target) {
       // ceate a new var to store the result
       var translation = null;
       translate
         .translate(textToTranslate, target)
         .then(results => {
           translation = results[0];
           return translation;
         })
         .catch(err => {
           console.error('ERROR:', err);
         });
       // this should now the translation:
       console.log(translation)
       return translation;
     }
*/