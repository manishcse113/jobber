var Otp = require('../models/OtpSchema.js');
var Consumer = require('../models/UserSchema.js');
var Sman = require('../models/ServiceManSchema.js');
var ConsumerController = require('./consumer.controller.js');
var SmanController = require('./sman.controller.js');
// Create and Save 
var querystring = require('querystring');
var http = require('http');

exports.create = (req, res) => {
	console.log("Otp.controller create");
    // Validate request
    if(!req.body.phone) {
        return res.status(400).send({
            message: "Otp create -phone can not be empty"
        });
    }
	
	if(!req.body.userRole) {
        return res.status(400).send({
            message: "Otp create -userType can not be empty"
        });
    }
	
	/*
	else if(!req.body.otp) {
        return res.status(400).send({
            message: "Otp create -otp can not be empty"
        });
    }
	*/
	else
	{
		var random = randomValueHex(4)  // value 'ad0fc8c'

		/*
		// this is the functionality i like the most 
		var rn = require('random-number');
		var options = {
		  min:  1000
		, max:  10000
		, integer: true
		}
		rn(options) // example outputs → -187, 636 
		*/		
		console.log("Checking if consumer already exists " + "phone #"+ req.body.phone);

		if (req.body.userRole==="Consumer")
		{
			Consumer.find(
			{
				$and:[{phone:req.body.phone}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				console.log(result);
				if(result.length === 0) {
					console.log("user doesn not exists" + result);
					return res.status(404).send({
						message: "User does not Exists - select correct user phone" 
					});
				}		
				else
				{
					console.log("User exists - creating a new Otp");
					//If everything ok, create a new record::
					console.log(result);
					//Create a serviceman					
					var otp = new Otp();
					otp.otp = random;
					otp.phone = req.body.phone;
					sendOtp(otp.otp, otp.phone);
					otp.save(function(err, result){
						if (err)
						{
							return res.send(err);
						}
						console.log("otp create success" + result);
						res.send({
								message: "otp created succcess" ,
								phone: result.phone,
								_id: result._id
						});
						//message:'user created';
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating otp " + req.params.phone
							});                
						}
						return res.status(500).send({
							message: "Error creating otp " + req.params.phone
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "otp could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error otp could not be created"
				});
			});
		}
		else
		{
			Sman.find(
			{
				$and:[{phone:req.body.phone}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				console.log(result);
				if(result.length === 0) {
					console.log("user doesn not exists" + result);
					return res.status(404).send({
						message: "User does not Exists - select correct user phone" 
					});
				}		
				else
				{
					console.log("User exists - creating a new Otp");
					//If everything ok, create a new record::
					console.log(result);
					//Create a serviceman					
					var otp = new Otp();
					otp.otp = random;
					otp.phone = req.body.phone;
					sendOtp(otp.otp, otp.phone);
					otp.save(function(err, result){
						if (err)
						{
							return res.send(err);
						}
						console.log("otp create success" + result);
						res.send({
								message: "otp created succcess" ,
								phone: result.phone,
								_id: result._id
						});
						//message:'user created';
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating otp " + req.params.phone
							});                
						}
						return res.status(500).send({
							message: "Error creating otp " + req.params.phone
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "otp could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error otp could not be created"
				});
			});
		}
	}
};

// validate
exports.validate = (req, res) => {
  console.log("validate otp" + req);
    Otp.find(
		{
			$and: [	{phone: req.params.phone},{otp: req.params.otp}]
		}
		,
		{ _id: 1, phone: 1 }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check OTP details for " + req.params.phone
				});
			}
			else{
				
				if(req.params.userRole==="Consumer")
				{
					//mark consumer as verified
					ConsumerController.markVerified(req.params.phone);
				}
				else
				{
					//mark serviceman as verified
					SmanController.markVerified(req.params.phone);
				}
				res.send({
					message:"Otp validate succuess",
					isVerified:true,
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Otp not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Otp with phone " + req.params.phone
        });
    });
};


// clearTimeoutOtp
exports.clearTimeoutOtp = () => {
    console.log("clearing timedout OTP's");
	var tenMinutesOld = new Date();
	tenMinutesOld.setMinutes(tenMinutesOld.getMinutes()-6);
	
	Otp.remove( 
		{createdAt: {$lt:tenMinutesOld}}, 
		function(err, result) 
		{
			if (err) {
				//console.log(result);
			}					
			if(result.length===0) {
				
			}
			else{
				//console.log(result);
		
			}
		}
	);
};


var crypto = require('crypto');

function randomValueHex (len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
}

function sendOtp(otp, phone)
{
	var messageToSend =  'Jobber Otp# ' + otp;
	sendSms(messageToSend, phone);
}

exports.sendPassword = (password, phone) =>
//function sendPassword(password, phone)
{	console.log("sending password" + password);
	var messageToSend =  'Jobber Password# ' + password;
	sendSms(messageToSend, phone);
	console.log("sms sent");
}

exports.sendMessage = (msg, phone) =>
//function sendPassword(password, phone)
{	console.log("sending message" + msg);
	var messageToSend =  'Jobber msg# ' + msg;
	sendSms(messageToSend, phone);
	console.log("sms sent");
}

function sendSms(messageToSend, phone)
{
	sendSmSTextLocal(messageToSend, phone);
	//sendSmSInternationalBulkSMS(messageToSend, phone);
}

var http = require("http");
function sendSmSTextLocal(messageToSend, phone)
{
		//Send OTP to mobile:: BulkSMS
	//var auth = 'Basic ' + new Buffer("7566C3C5C0D54518B07A9731D41FD0D0-02-5:DILmf_C2gtD0rEL*zigtN84f*lO5r").toString('base64');
	var post_options = {
		host: 'api.textlocal.in',
		port: 80,
		method:'POST',
		path: '/send',
		headers:{
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	};
	
	// Build the post string from an object
	var post_data = querystring.stringify({
		'numbers':phone,
		'sender': 'TXTLCL',
		'message':messageToSend,
		'apikey':'nNTASELZGbc-bVDpCBakafjXWaasNhXfKk5Z51RN9S'
	});
	
	// Set up the request
	var post_req = http.request(post_options, function(res) {
	  res.setEncoding('utf8');
	  res.on('data', function (chunk) {
		  console.log('Response: ' + chunk);
	  });
	});
	// post the data
	post_req.write(post_data);
	post_req.end();
}

function sendSmSInternationalBulkSMS(messageToSend, phone)
{
	//Send OTP to mobile:: BulkSMS
	var auth = 'Basic ' + new Buffer("7566C3C5C0D54518B07A9731D41FD0D0-02-5:DILmf_C2gtD0rEL*zigtN84f*lO5r").toString('base64');
	var post_options = {
		host: 'api.bulksms.com',
		port: 80,
		method:'POST',
		path: '/v1/messages',
		headers:{
			'Content-Type': 'application/json',
			"Authorization": auth
		}
	};
	
	// Build the post string from an object
	var post_data = querystring.stringify({
		'to':phone,
		'body': messageToSend
	});
	
	// Set up the request
	var post_req = http.request(post_options, function(res) {
	  res.setEncoding('utf8');
	  res.on('data', function (chunk) {
		  console.log('Response: ' + chunk);
	  });
	});
	// post the data
	post_req.write(post_data);
	post_req.end();				
}
//var value1 = randomValueHex(12) // value 'd5be8583137b'
//var value2 = randomValueHex(2)  // value 'd9'

