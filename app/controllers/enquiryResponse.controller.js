var EnquiryResponse = require('../models/enquiryResponseSchema.js');
var Consumer = require('../models/UserSchema.js');
// Create and Save 
exports.addResponse = (req, res) => {
	console.log("addResponse.controller create");
    // Validate request
	if(!req.body.enquiryId) {
		isJobId = false;
		if(!req.body.enquiryId) {		
			return res.status(400).send({
				message: "Response create - enquiryId is empty"
			});
		}
		
    }
	else if(!req.body.senderId) {
        return res.status(400).send({
            message: "Response create -senderId can not be empty"
        });
    }
	else if(!req.body.senderUserName) {
        return res.status(400).send({
            message: "Response create -senderUserName can not be empty"
        });
    }
	else if(!req.body.senderPhone) {
        return res.status(400).send({
            message: "Response create -senderPhone can not be empty"
        });
    }
	else if(!req.body.isConsumer) {
        return res.status(400).send({
            message: "Response create -isConsumer can not be empty"
        });
    }
	else if(!req.body.msg) {
        return res.status(400).send({
            message: "Response create -msg can not be empty"
        });
    }
	else
	{			
		console.log("Enquiry saving record ");
		var response = new EnquiryResponse();
		response.enquiryId = req.body.enquiryId;
		
		response.senderId = req.body.senderId;
		response.senderUserName = req.body.senderUserName;
		response.senderPhone = req.body.senderPhone;
		response.senderFirstName = req.body.senderFirstName;
		response.msg = req.body.msg;
		response.parentResponseId = req.body.parentResponseId;
		response.isConsumer = req.body.isConsumer;		
		response.save(function(err, result){
			if (err)
			{
				return res.send(err);
			}
			console.log("Response create success" + result);
			res.send({
					message: "success" ,
					result
			});
		}).
		catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Error creating Response +EnquiryId " + req.body.enquiryId + 'msg:' + req.body.msg
				});                
			}
			return res.status(500).send({
				message: "Error creating Response +enquiryId " + req.body.jobId + 'msg:' + req.body.msg
			});						
		});
	}
};


// get Responses by me
exports.getResponseByMe = (req, res) => {
	console.log("getResponse senderId " + req);
    EnquiryResponse.find(
		{
			$and: [	{senderId: req.params.senderId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check senderId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Reseponse not found with senderId " + req.params.senderId
            });                
        }
        return res.status(500).send({
            message: "Reseponse not found with senderId " + req.params.senderId
        });
    });
};


// get Responses by EnquiryId
exports.getResponseByEnquiry = (req, res) => {
	console.log("getResponse enquiryId " + req);
    EnquiryResponse.find(
		{
			$and: [	{enquiryId: req.params.enquiryId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check enquiryId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Reseponse not found with enquiryId " + req.params.enquiryId
            });                
        }
        return res.status(500).send({
            message: "Reseponse not found with enquiryId " + req.params.enquiryId
        });
    });
};

// get Responses by responseId
exports.getChildResponses = (req, res) => {
	console.log("getResponse responseId " + req);
    EnquiryResponse.find(
		{
			$and: [	{parentResponseId: req.params.responseId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check parentResponseId "
				});
			}
			else{
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Reseponse not found with responseId " + req.params.responseId
            });                
        }
        return res.status(500).send({
            message: "Reseponse not found with enquiryId " + req.params.enquiryId
        });
    });
};