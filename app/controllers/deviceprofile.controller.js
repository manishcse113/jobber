/* Created by Manish */

var DeviceProfile = require('../models/DeviceProfileSchema.js');

// Create and Save 
exports.addDeviceProfile = (req, res) => {
	console.log("addDeviceProfile.controller create " + req.body);
    // Validate request
    if(!req.body.macId) {
        return res.status(400).send({
            message: "Device Profile create -macId can not be empty"
        });
    }
	else if(!req.body.morStartHr) {
        return res.status(400).send({
            message: "Device Profile create -morStartHr can not be empty"
        });
    }
	else if(!req.body.morStartMin) {
        return res.status(400).send({
            message: "Device Profile create -morStartMin can not be empty"
        });
    }
	else if(!req.body.eveStartHr) {
        return res.status(400).send({
            message: "Device Profile create -eveStartHr can not be empty"
        });
    }
	else if(!req.body.eveStartMin) {
        return res.status(400).send({
            message: "Device Profile create -eveStartMin can not be empty"
        });
    }
	else if(!req.body.retries) {
        return res.status(400).send({
            message: "Device Profile retries can not be empty"
        });
    }
	else if(!req.body.retryInterval) {
        return res.status(400).send({
            message: "Device Profile create -retryInterval can not be empty"
        });
    }
	else if(!req.body.noWaterStopTime) {
        return res.status(400).send({
            message: "Device Profile create -noWaterStopTime can not be empty"
        });
    } else {	
		console.log("#################DeviceProfile saving record ###################");
		var deviceProfile = new DeviceProfile();
		deviceProfile.macId = req.body.macId;
		deviceProfile.morStartHr = req.body.morStartHr;
		deviceProfile.morStartMin = req.body.morStartMin;
		deviceProfile.eveStartHr = req.body.eveStartHr;
		deviceProfile.eveStartMin = req.body.eveStartMin;
		deviceProfile.retries = req.body.retries;
		deviceProfile.retryInterval = req.body.retryInterval;
		deviceProfile.noWaterStopTime = req.body.noWaterStopTime;
		
		deviceProfile.save(function(err, result2){
			if (err) {
				return res.send(err);
			}
			console.log("Success" + result2);
			res.send({
					message: "Success" ,
					result2
			});
		}).
		catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "Error creating deviceProfile mac " + req.body.macId 
				});                
			}
			return res.status(500).send({
				message: "Error creating deviceProfile MacId " + req.body.macId 
			});						
		});
	}
};


// get Device by macId
exports.getDeviceProfilebyMacId = (req, res) => {
	console.log("getDeviceProfile macId " + req);
    DeviceProfile.find(
		{
			$and: [	{macId: req.params.macId}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check deviceId "
				});
			}
			*/	
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeviceProfile not found with macId " + req.params.macId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving DeviceProfile with macId " + req.params.macId
        });
    });
};



// get All Events List
exports.getAllDeviceProfiles = (req, res) => {
	console.log("getDevicesProfiles " + req);
    DeviceProfile.find(
		{
			//$and: [	{userId: req.params.userId, status:req.params.status}]
		}
		//,
		//{ _id: 1, }
		, 
		function (err, result) 
		{
			if (err) throw err;		
			
			/*
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check userId "
				});
			}
			*/
			else{
				console.log(result);
				res.send({
					message:"Success",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeviceProfile not found "
            });                
        }
        return res.status(500).send({
            message: "DeviceProfile could not be retrieved" 
        });
    });
};
