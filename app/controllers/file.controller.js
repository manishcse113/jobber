var path = require('path');     //used for file path
var fs =require('fs-extra');    //File System-needed for renaming file etc
var multer  = require('multer');
var upload6 = multer({ dest: 'upload/'});
var fs = require('fs');
/** Permissible loading a single file, 
	the value of the attribute "key" in the form of "image". **/
var type = upload6.single('image');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.post('/Upload', type, function (req,res) 
//exports.upload = (req, res) =>
{
	  /** When using the "single"
		  data come in "req.file" regardless of the attribute "name". **/
	  var tmp_path = req.file.path;
	  /** The original name of the uploaded file
		  stored in the variable "originalname". **/
	  var target_path = 'upload/' + req.file.originalname;
	  /** A better way to copy the uploaded file. **/
	  var src = fs.createReadStream(tmp_path);
	  var dest = fs.createWriteStream(target_path);
	  src.pipe(dest);
	  src.on('end', function() { 
			//res.render('complete'); 
			console.log("End");
			res.send();
		});
	  src.on('error', function(err) { 
			console.log("error");
			res.send();
			//res.render('error'); 
		});
};

