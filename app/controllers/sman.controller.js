//const Jobber = require('../models/JobberSchema.js');
var Sman = require('../models/ServiceManSchema.js');
var SmanFav = require('../models/ServiceManFavouriteSchema.js');
var OtpController = require('./otp.controller.js');
var Favourite = require('../models/FavouriteSchema.js');
var db = require('../../server2.js');
var Consumer = require('../models/UserSchema.js');

// Create and Save 
exports.createService = (req, res) => {
	  console.log("sman.controller createService");
	 // message: "createService success " ;//+ req.body.serviceName;
	  //res.send(message);		
};


// Create and Save 
exports.create = (req, res) => {
	  console.log("sman.controller create");
    // Validate request
    if(!req.body.phone) {
        return res.status(400).send({
            message: "sMan create -phone can not be empty"
        });
    }
	else if(!req.body.userName) {
        return res.status(400).send({
            message: "sMan create -userName can not be empty"
        });
    }
	else if(!req.body.password) {
        return res.status(400).send({
            message: "sMan create -password can not be empty"
        });
    }
	else if(!req.body.serviceName) {
        return res.status(400).send({
            message: "sMan create- serviceName can not be empty"
        });
    }
	
	else
	{
		console.log("Checking if user already exists " + req.params);
		//Find that userName is not taken & phone is not already taken
		//$and: [	{userName: req.params.userName} ]
		Sman.find(
			{
				$or:[{userName:req.body.userName},{phone:req.body.phone}]
			}
			, 
			function (err, result) 
			{
				if (err) throw err;
				console.log(result);
				if(result.length !== 0) {
					console.log("user already exists" + sman);
					return res.status(404).send({
						message: "Serviceman Already Exists - Choose new credentials or click forget Password" 
					});
				}
				else
				{
					console.log("user doesn't exists - creating new");
					//If everything ok, create a new record::
					console.log(req.body.userName);
					//Create a serviceman
					
					var sman = new Sman();
					sman.userName = req.body.userName;
					sman.serviceName = req.body.serviceName;
					sman.password = req.body.password;
					sman.phone = req.body.phone;
					sman.email = '';
					sman.state = '';
					if (!req.body.city)
					{
						sman.city =  '';
					}
					else
					{
						sman.city =  req.body.city;
					}
					if(!req.body.location)
					{
						sman.location = '';
					}
					else
					{
						sman.location = req.body.location;
					}
					sman.pin = '';
					sman.firstName = '';
					sman.lastName = '';
					sman.isVerified = false;

					sman.likeCount = 0;
					sman.photoUrl = '';
						
					sman.save(function(err, savedSMan){
						if (err)
						{
							return res.send(err);
						}
						console.log("serviceman create success" + savedSMan.phone);
						//res.send("user created");
						res.send({
								message: "succcess" ,
								userName: savedSMan.userName,
								isVerified: result.isVerified,
								_id: savedSMan._id
						});
						//message:'user created';
					}).
					catch(err => {
						if(err.kind === 'ObjectId') {
							return res.status(404).send({
								message: "Error creating user " + req.params.userName
							});                
						}
						return res.status(500).send({
							message: "Error creating user " + req.params.userName
						});
					});
				}
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "Serviceman could not be created"
					});                
				}
				return res.status(500).send({
					message: "Error Serviceman could not be created"
				});
			});
	}
};

// login
exports.login = (req, res) => {
  console.log("login" + req);
    Sman.find(
		{
			$and: [	{phone: req.params.phone},{password: req.params.password}]
		}
		,
		{ password: 0 }
		, 
		function (err, result) 
		{
			if (err) throw err;
				
			console.log(result);		
			if(result.length===0) {
				return res.status(404).send({
					message: "Please check login details for " + req.params.phone
				});
			}
			else{
				res.send({
					message:"login succuess",
					result
				});	
			}
        }
	)
	.catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Serviceman not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving serviceman with phone " + req.params.phone
        });
    });
};

exports.forgetByName = (req, res) => {
    Sman.find(
		{
			$and: [	{userName: req.params.userName} ]
		}
		,
		{ _id: 1, userName: 1, password : 1, phone:1 }
	)
    .then(sman => {
		console.log(sman);
        if(sman.length === 0) {
            return res.status(404).send({
                message: "Serviceman not found- " + req.params.userName
            });            
        }
		else
		{
			OtpController.sendPassword(sman[0].password, sman[0].phone);
				
			res.send({
					message:"Password has been sent"
				});	
			/*
			return res.status(200).send({
					message: sman
				});
			*/
		}
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Serviceman not found with userName " + req.params.userName
            });                
        }
        return res.status(500).send({
            message: "Error retrieving serviceman with userName " + req.params.userName
        });
    });
};

exports.forgetByPhone = (req, res) => {
 Sman.find(
		{
			$and: [	{phone: req.params.phone} ]
		}
		,
		{ _id: 1, userName: 1, password :1 }
	)
    .then(sman => {
        if(sman.length === 0) {
            return res.status(404).send({
                message: "Serviceman not found with phone " + req.params.phone
            });            
        }
		else
		{
				OtpController.sendPassword(sman[0].password, req.params.phone);
				return res.send({
					message:"Password has been sent"
				});	
				/*
				return res.status(200).send({
					message: sman
				}); */
		}
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Serviceman not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving serviceman with phone " + req.params.phone
        });
    });
};

exports.resetPassword = (req, res) => {
	console.log("resetPassword");
	Sman.find(
		{
			$and: [	{phone: req.params.phone}, {password:req.body.oldPassword} ]
		}
		,
		{ _id: 1, userName: 1 }
	)
    .then(sman => {
        if(sman.length === 0) {
            return res.status(404).send({
                message: "Serviceman not found with phone or old password " + req.params.phone
            });            
        }
		else
		{
			// Find Sman and update it with the request body
			Sman.findOneAndUpdate(
				{ "phone": req.params.phone },
				{ "$set":
					{
						password: req.body.newPassword
					}
				},
				{
					"fields": {"password":0},
					"new": true
				}
				)
				.then(sman => {
					console.log("found sman ", sman);
					if(sman.length===0) {
						return res.status(404).send({
							message: "sman not found with phone " + req.params.phone
					});
				}
				//res.send(sman);
				return res.send({
							message:"Success",
							sman
						});	
			}).catch(err => {
				if(err.kind === 'ObjectId') {
					return res.status(404).send({
						message: "SMan not found with phone " + req.params.phone
					});                
				}
				return res.status(500).send({
					message: "Error updating sman with phone " + req.params.phone
				});
			});
				/*
				return res.send({
					message:"OTP sent",
					sman
				});
				*/
				/*
				return res.status(200).send({
					message: sman
				}); */
		}
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Serviceman not found with phone " + req.params.phone
            });                
        }
        return res.status(500).send({
            message: "Error retrieving serviceman with phone " + req.params.phone
        });
    });
};

exports.findByCity = (req, res) => {
 let smen = [];//This is an array
 Sman.find(
		{
			$and: [	{city: req.params.city} ]
		}
		,
		{ password: 0 }
	)
    .then( async function (result){// => async {
		//console.log("findbyCity #" + result);
		//For each serviceman - get all userId/Names
		//compose a response and then send

		var i=0;
		for (l in result) {      
			var o = result[l];
			let sman = {};
			await Favourite.find(
			{
				$and: [	{smanId: o._id}]
			}
			,
			{ _id:0, smanId:0, updatedAt:0, createdAt:0, __v:0 }
			,
			await function (err, userIds) 
			{
				if (err) throw err;			
				else if(userIds.length===0) {
					sman.sman = o;
					sman.userIds = userIds;
					smen.push (sman);
					/*
					return res.status(200).send({
						message: "No record found "
					});
					*/
				}
				else{
					//store this in new result
					sman.sman = o;
					sman.userIds = userIds;
					smen.push (sman);
				}
			})
			i++
		}		
		return res.send({
					message:"Success",
					smen
				});	
		
    }).catch(err => {
		console.log(err);
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving smen."
        });
    });
};

exports.findByCityProf = (req, res) => {
	let smen = [];//This is an array
	Sman.find(
		{
			$and: [	{city: req.params.city}, {serviceName: req.params.serviceName} ]
		}
		,
		{ password: 0 },
		function(err, smen) {
			if (err) callback( err, result);
			else {
				if(smen.length===0) {
					return res.status(200).send({
						message: "No record found "
					});
				}
				else {
					res.send({ message:"Success", smen});
				}
			}
		}
	).
	catch(err => {
		console.log(" error in getting serviceman " + err);
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving smen."
        });
    });
};

var SmanBasic = {

//class SmanBasic
//{
//  constructor()
  userName: String,
  serviceName: String,
  password: String,
  phone: String,
  email: String,
  state: String,
  city: String,
  location: String,
  pin: String,
  firstName: String,
  lastName: String,
  isVerified: Boolean,
  likeCount: Number,
  photoUrl: String
}

//new response structure for find::
SmanComposite =
{
  sman:SmanBasic,
  likingUserIds:[], //[],//: String[],
  //likingUserNames:[],//:String[]  
}

exports.findByCityProfLoc = (req, res) => {
	let smen = [];//This is an array
	Sman.find(
		{
			$and: [	{city: req.params.city}, {serviceName: req.params.serviceName}, {location: req.params.location} ]
		}
		,
		{ password: 0 }
	)
    .then( async function (result){// => async {
		//console.log("findbyCity #" + result);
		//For each serviceman - get all userId/Names
		//compose a response and then send

		var i=0;
		for (l in result) {      
			var o = result[l];
			let sman = {};
			await Favourite.find(
			{
				$and: [	{smanId: o._id}]
			}
			,
			{ _id:0, smanId:0, updatedAt:0, createdAt:0, __v:0 }
			,
			await function (err, userIds) 
			{
				if (err) throw err;			
				else if(userIds.length===0) {
					sman.sman = o;
					sman.userIds = userIds;
					smen.push (sman);
					/*
					return res.status(200).send({
						message: "No record found "
					});
					*/
				}
				else{
					//store this in new result
					sman.sman = o;
					sman.userIds = userIds;
					smen.push (sman);
				}
			})
			i++
		}		
		return res.send({
					message:"Success",
					smen
				});	
		
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving smen."
        });
    });
};

exports.findAllByCity = (req, res) => {


	
	Sman.find(
		{
			$and: [	{city: req.params.city} ]
		}
		,
		{ password: 0 }
	)
    .then(smen => {
        //res.send(smen);
		return res.send({
					message:"Success",
					smen
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving smen."
        });
    });
};

exports.findAllByLocation = (req, res) => {
	Sman.find(
		{
			$and: [	{city: req.params.city}, {location: req.params.location} ]
		}
		,
		{ password: 0 }
	)
    .then(smen => {
        //res.send(smen);
		return res.send({
					message:"Success",
					smen
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving smen."
        });
    });
};

exports.findAll = (req, res) => {
	
	/*
	SmanFav.aggregate([
	{ $lookup: { from: Sman, localField: "_id", foreignField: "smanId", as: "Sman" } }, 
	{ $lookup: { from: Favourite, localField: "smanId", foreignField: "smanId", as: "Favourite" } }, 
	{ $unwind: "$Sman"},
	{ $unwind: "$Favourite"},
	{$project: {_id:0}} ]);
	*/
	Sman.find(
			{},
			{ password: 0 }
		)
		
		.then(smen => {
        //res.send(smen);
		return res.send({
					message:"Success",
					smen
				});	
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving smen."
        });
    });
};

exports.updateProfile = (req, res) => {
	// Validate Request
    if(!req.body.city) {
		console.log("not found with city");
        return res.status(400).send({
            message: "SMan Profile city can not be empty"
        });
    }
    // Find Sman and update it with the request body
    Sman.findOneAndUpdate(
		{ "_id": req.params.sManId },
		{ "$set":
			{
				state: req.body.state,
				city: req.body.city,
				location: req.body.location,
				pin: req.body.pin,
				email: req.body.email,
				firstName: req.body.firstName,
				lastName: req.body.lastName
			}
		},
		{
			"fields": {"password":0},
			"new": true
		}
		)
		.then(sman => {
			console.log("found sman ", sman);
			if(sman.length===0) {
				return res.status(404).send({
					message: "sman not found with id " + req.params.sManId
			});
        }
        //res.send(sman);
		return res.send({
					message:"Success",
					sman
				});	
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "SMan not found with id " + req.params.sManId
            });                
        }
        return res.status(500).send({
            message: "Error updating sman with id " + req.params.sManId
        });
    });
};

exports.updateProfilePic = (req, res) => {
    // Find sman and update it with the request body
    Sman.findOneAndUpdate(
		{ "_id": req.params.contextId },
		{ "$set":
			{
				photoUrl: req.file.originalname
			}
		},
		{
			"fields": {"password":0},
			"new": true
		}
		)
		.then(sman => {
			console.log("found sman ", sman);
			if(sman.length===0) {
				return res.status(404).send({
					message: "sman not found with id " + req.params.contextId
			});
        }
		return res.send({
					message:"Success",
					sman
				});	
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "sman not found with id " + req.params.contextId
            });                
        }
        return res.status(500).send({
            message: "Error updating sman with id " + req.params.contextId
		})
        
    });
};


exports.updateLike = (req, res) => {
	
	// Validate Request
    if(!req.body.like) {
        return res.status(400).send({
            message: "SMan Profile like can not be empty"
        });
    }
	var flag=1;
	if (req.body.like==="true")
	{
		console.log("updateLike found true");
		Sman.findOneAndUpdate(
			{_id:req.params.sManId}, 
			{
				$inc: {	likeCount : 1}
			},
			{
				"fields": {"password":0},
				"new": true
			}
		)
		.then(sman => {
			console.log(sman);
			if(sman.length===0) {
				return res.status(404).send({
					message: "sman not found " + sman
				});
			}
			//res.send(sman);
			return res.send({
					message:"Success",
					sman
				});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "SMan not found with phone " + req.params.phone
				});                
			}
			return res.status(500).send({
				message: "Error updating sman with phone " + req.params.phone
			});
		});	
	}
	else
	{
		flag = -1;
		Sman.findOneAndUpdate(
			{_id:req.params.sManId}, 
			{
				$inc: {	likeCount : -1}
			},
			{
				"fields": {"password":0},
				"new": true
			}
		)
		.then(sman => {
			console.log(sman);
			if(sman.length===0) {
				return res.status(404).send({
					message: "sman not found " + sman
				});
			}
			//res.send(sman);
			return res.send({
					message:"Success",
					sman
				});	
		}).catch(err => {
			if(err.kind === 'ObjectId') {
				return res.status(404).send({
					message: "SMan not found with sManId " + req.params.sManId
				});                
			}
			return res.status(500).send({
				message: "Error updating sman with sManId " + req.params.sManId
			});
		});	
	}	
};


exports.updateLikeLocal = (smanId, userId, likeFlag) => {
	
	var flag=1;
	if (likeFlag==="true")
	{
		console.log("updateLike found true " +userId );
		//getUserName from user ID, add to favourite - it not already exists
		Consumer.findOne(		
			{ $and: [	{_id: userId} ]	}
			,
			{ userName: 1 }
			).then (user => {
				console.log("found user with userId userName " + user._id + "#" + user.userName);
				//add in the array of users for this sman
				var likedingUser = { userName: user.userName, _id: userId };
				//Sman.likedby.push(likedby);
				//Sman.save(done);
				//increment rating
				Sman.findOneAndUpdate(
					{_id:smanId}, 
					{
						$inc: {	likeCount : 1}
						,
						$push: { likedby: likedingUser }
					},
					{
						"fields": {"password":0},
						"new": true
					}
				)
				.then(sman => {
					console.log(sman);
					if(sman.length===0) {
						console.log("favourite count incremented failure - record not found");
						return ;
					}
					console.log("favourite count incremented success");
					//res.send(sman);
					return ;
				}).catch(err => {
					console.log("favourite count incremented failure - exception #" + err);
					if(err.kind === 'ObjectId') {		
						return ;
					}
					return ;
				});		
			}).catch(err => {
				console.log("No user found with this id");
				if(err.kind === 'ObjectId') {					
					return ;
				}
				return ;
			});
	}
	else
	{
		flag = -1;
		console.log("updateLike found true " +userId );
		//getUserName from user ID, add to favourite - it not already exists
		Consumer.findOne(		
			{ $and: [	{_id: userId} ]	}
			,
			{ userName: 1 }
			).then (user => {
				console.log("found user with userId userName " + user._id + "#" + user.userName);
				//add in the array of users for this sman
				var removelikeUser = { userName: user.userName, _id: userId };
				//Sman.likedby.push(likedby);
				//Sman.save(done);
				//increment rating
				Sman.findOneAndUpdate(
					{_id:smanId}, 
					{
						$inc: {	likeCount : -1}
						,
						$pull: { likedby: removelikeUser }
					},
					{
						"fields": {"password":0},
						"new": true
					}
				)
				.then(sman => {
					console.log(sman);
					if(sman.length===0) {
						console.log("favourite count decrement failure - record not found");
						return ;
					}
					console.log("favourite count decrement success");
					//res.send(sman);
					return ;
				}).catch(err => {
					console.log("favourite count decrement failure - exception #" + err);
					if(err.kind === 'ObjectId') {		
						return ;
					}
					return ;
				});		
			}).catch(err => {
				console.log("No user found with this id");
				if(err.kind === 'ObjectId') {					
					return ;
				}
				return ;
			});
	}	
};


exports.markVerified = (phone) => {
	// Validate Request
    // Find Consumer and update it with the request body
    Sman.findOneAndUpdate(
		{ "phone": phone },
		{ "$set":
			{
				isVerified: true
			}
		},
		{
			"fields": {"password":0},
			"new": true
		}
		)
		.then(consumer => {
			console.log("Marked consumer as verified", consumer);
		}).catch(err => {
			console.log("Could not mark consumer as verified");
		});
};