// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

var cors=require('./cors');
app.use(cors.permission);

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080	; // set our port

// DATABASE SETUP
var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
mongoose.connect('mongodb://localhost:27017/myjobs');


// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});


var Schema = mongoose.Schema;
var JobSchema   = new Schema(
	{
	jobId: Number,
	jobcategory : String,
	jobtitle: String,
	description : String,
	posting_date : Date,
	status : String,
	consumerId : String,
	city : String,
	location : String

});
var Jobs=mongoose.model('Jobs',JobSchema);
module.exports = Jobs;

mongoose.Promise=require('bluebird');

		var jobsm = new Jobs();

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /bears
// ---------------------------------------------------
router.route('/job')

	// create a job (accessed at POST http://localhost:8080/job)
	.post(function(req, res) {
		console.log('Post Job received');
		// create a new instance of the Jobs model
		var jmax = 0;
		Jobs.findOne().sort('-jobId').exec( function(err, jobsm) {
			console.log('jobid=');
			console.log(jobsm.jobId);
			jmax=jobsm.jobId + 1;
			console.log('jmax=');
		console.log(jmax);
		//jobs.jobId = req.body.jobId;
		var jobs=new Jobs();
		jobs.jobId=jmax;
		jobs.jobcategory = req.body.category;
		jobs.jobtitle = req.body.jobtitle;
		jobs.description = req.body.description;
		jobs.posting_date = req.body.posting_date;
		jobs.status = req.body.status;
		jobs.consumerId=req.body.consumerId;
		jobs.city=req.body.city;
		jobs.location=req.body.location;
		
		console.log(req.body.posting_date);

		jobs.save(function(err) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
				
			res.json({ message: 'Jobs created!' });

			//res.send(JSON.stringify({a:1}, null, 3));
		});

	 });
	})

	// get all the jobs (accessed at GET http://localhost:8080/api/job)
	.get(function(req, res) {
		Jobs.find(function(err, jobs) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
		console.log('Get All');
		//console.log(req);
			res.json(jobs);
			
			//res.send(JSON.stringify({a:1}, null, 3));
			console.log("Sending response as");
			//console.log(res);
		});
	});

		// get all the jobs in a city (accessed at GET http://localhost:8080/api/job/city/:city)
	 router.route('/job/city/:city')
	.get(function(req, res) {
		console.log(req.params.city);
		Jobs.find({city : req.params.city},function(err, jobs) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
			console.log('Getting City..');
			
			res.json(jobs);
			//res.send(JSON.stringify({a:1}, null, 3));
		});
	});
	
     //get all jobs in a city - location
	router.route('/job/:city/:location')
	.get(function(req, res) {
		console.log(req.params.location);
		Jobs.find({locatiion : req.params.location},function(err, jobs) {
			if (err)
				res.send(err);
			console.log('Getting Location..');
			res.json(jobs);
		});
	});
	
		router.route('/job/consumer/:consumerId')
	.get(function(req, res) {
		console.log(req.params.consumerId);
		Jobs.find({consumerId : req.params.consumerId},function(err, jobs) {
			if (err)
				res.send(err);
			console.log('Getting ConnsumerID..');
			res.json(jobs);
		});
	});
	
	
	// get all the jobs in a category, city and location
	 router.route('/job/:jobcategory/:city/:location')

	.get(function(req, res) {
		console.log(req.params.jobcategory);
		Jobs.find({jobcategory : req.params.jobcategory, city : req.params.city, serviceName : req.params.serviceName, location : req.params.location},function(err, jobs) {
			res.setHeader('Content-Type', "application/json")
			if (err)
				res.send(err);
			console.log('Getting City and Job category..');
			res.json(jobs);
			
			//res.send(JSON.stringify({a:1}, null, 3));
		});
	});
	



// update Job Status
	router.route('/job/jobId/:status')
	.put(function(req, res) {
		Jobs.findById(req.params.jobId, function(err, jobs) {

			if (err)
				res.send(err);

			jobs.status=req.params.status;
			jobs.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Job Status updated!' });
			});

		});
	})

	// delete the consumer with this id
//	.delete(function(req, res) {
//		Consumer.remove({
//			_id: req.params.consumer_id
//		}, function(err, consumer) {
//			if (err)
//				res.send(err);

//			res.json({ message: 'Successfully deleted' });
//		});
//	});



// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
