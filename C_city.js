// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080	; // set our port

// DATABASE SETUP
var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
mongoose.connect('mongodb://localhost:27017/Job_main');

// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});

// Consumer Routes
var Consumer     = require('./app/models/consumer');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening in get/consumer/city.');
	next();
});


// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// ----------------------------------------------------
//router.route('/consumers/city/')

	

		// get all the consumers in a city (accessed at GET http://localhost:8080/api/consumers/city/:city)

	router.get('/consumers/city/:cityName',function(req, res) {
		console.log('Test1');
		console.log(req.params.cityName);
		console.log('Test2');
		
		Consumer.find({Ccity : req.params.cityName},function(err, consumers) {
			if (err)
				res.send(err);

			res.json(consumers);
		});
		
	});
	



	// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
